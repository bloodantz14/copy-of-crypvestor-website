@component('mail::message')
# Hey guys, new message from contact form... <br>
<div>Name: {{$contact->name}}</div>
<div>Email: {{$contact->email}}</div>
<div>Country: {{$contact->country}}</div>
<div>Message: {{$contact->message}}</div>

{{-- Subcopy --}}
@isset($actionText)
    @slot('subcopy')
        @lang(
            "If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\n".
            'into your web browser:',
            [
                'actionText' => $actionText,
            ]
        ) <span class="break-all">[{{ $displayableActionUrl }}]({{ $actionUrl }})</span>
    @endslot
@endisset
@endcomponent
