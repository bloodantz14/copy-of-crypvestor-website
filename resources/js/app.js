import Vue from 'vue'
import store from '~/store'
import router from '~/router'
import i18n from '~/plugins/i18n'
import App from '~/components/App'
import $ from 'jquery'
import Permissions from './mixins/permissions'

import { BootstrapVue } from 'bootstrap-vue'
import VueYoutube from 'vue-youtube'

import Scrollspy from 'vue2-scrollspy';
var VueScrollTo = require('vue-scrollto');


window.jQuery = $
window.$ = $

import '~/plugins'
import '~/components'

Vue.config.productionTip = false

// Install BootstrapVue
Vue.use(BootstrapVue)
Vue.use(VueYoutube)
Vue.use(Scrollspy)
Vue.use(VueScrollTo)


Vue.mixin(Permissions)

/* eslint-disable no-new */
new Vue({
  i18n,
  store,
  router,
  ...App
})
