function page(path) {
    return () => import(/* webpackChunkName: '' */ `~/pages/${path}`).then(m => m.default || m)
}

export default [
    {path: '/', name: 'welcome', component: page('welcome.vue')},
    {path: '/faq', name: 'faq', component: page('faq.vue')},
    {path: '/intelligence', name: 'intelligence', component: page('intelligence.vue')},
    {path: '/contact-us', name: 'contact-us', component: page('contact-us.vue')},
    {path: '/terms-conditions', name: 'terms-conditions', component: page('terms-conditions.vue')},
    {path: '/privacy', name: 'privacy', component: page('privacy.vue')},
    {path: '/referral-program', name: 'referral-program', component: page('referral-program.vue')},

    {path: '/login', name: 'login', component: page('auth/login.vue')},
    {path: '/register', name: 'register', component: page('auth/register.vue')},
    {path: '/password/reset', name: 'password.request', component: page('auth/password/email.vue')},
    {path: '/password/reset/:token', name: 'password.reset', component: page('auth/password/reset.vue')},
    {path: '/email/verify/:id', name: 'verification.verify', component: page('auth/verification/verify.vue')},
    {path: '/email/resend', name: 'verification.resend', component: page('auth/verification/resend.vue')},
    {
        path: '/dashboard',
        component: page('dashboard/index.vue'),
        children: [
            {path: '', redirect: {name: 'dashboard.home'}},
            {path: '/', name: 'dashboard.home', component: page('dashboard/dashboard.vue')},
            {path: 'my-accounts', name: 'dashboard.my-accounts', component: page('dashboard/my-accounts.vue')},
            {path: 'add-account', name: 'dashboard.add-account', component: page('dashboard/add-account.vue')},
            {path: 'referral-program', name: 'dashboard.referral-program', component: page('dashboard/referral-program.vue')},
            {path: 'profile', name: 'dashboard.profile', component: page('dashboard/profile.vue')},
            {path: 'security', name: 'dashboard.password', component: page('dashboard/password.vue')},

            // admin
            {path: 'all-accounts', name: 'dashboard.all-accounts', component: page('dashboard/admin-all-accounts.vue')},
            {path: 'trading', name: 'dashboard.trading', component: page('dashboard/trading.vue')},
            {path: 'trading-logs', name: 'dashboard.trading-logs', component: page('dashboard/trading-logs.vue')},
            {path: 'trading-futures-income', name: 'dashboard.futures-income', component: page('dashboard/trading-futures-incomes.vue')}
        ]
    },

    {path: '*', component: page('errors/404.vue')}
]
