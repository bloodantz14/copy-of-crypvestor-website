// import axios from 'axios'
// import Cookies from 'js-cookie'

const permissions = {

    data () {
        return {}
    },
    created: function () {
    },
    methods: {
        debugMode: function () {
            //return process.env.MIX_APP_DEBUG === 'true'
        },
        isClient: function () {
            let currentUser = this.$store.state.auth.user
            return currentUser.role === 'client'
        },
        isAdmin: function () {
            let currentUser = this.$store.state.auth.user
            return currentUser.role === 'admin'
        },


        // loginAs (userId) {
        //   axios.post('/api/login-as', { user_id: userId }).then((response) => {
        //     if (response.data.token) {
        //       Cookies.remove('token')
        //       Cookies.set('token', response.data.token, { expires: response.data.remember ? 365 : null })
        //       window.location.href = '/'
        //     }
        //   }).catch((error) => {
        //     console.log('LOG Error', error)
        //   })
        // },
        // loginAsIcon () {
        //   return '&#8669;'
        // }
    }

}

export default permissions
