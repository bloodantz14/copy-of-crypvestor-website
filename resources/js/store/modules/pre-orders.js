import axios from "axios";

// state
export const state = {
  all: []
}

// getters
export const getters = {
  all: state => state.all
}

// mutations
export const mutations = {
    setPreOrders(state, pre_orders) {
        state.all = pre_orders
    }
}

// actions
export const actions = {
    async getPreOrders({commit}) {
        const {data} = await axios.get('/api/admin/pre-orders')
        commit('setPreOrders', data)
    },
}

