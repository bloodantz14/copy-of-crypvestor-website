import axios from "axios";

// state
export const state = {
    all: [],
    order_groups: []
}

// getters
export const getters = {
    all: state => state.all,
    order_groups: state => state.order_groups
}

// mutations
export const mutations = {
    setOrderGroups(state, order_groups) {
        state.order_groups = order_groups
    },
    setOpenOrders(state, pre_orders) {
        state.all = pre_orders
    }
}

// actions
export const actions = {
    async getOrderGroups({commit}) {
        const {data} = await axios.get('/api/admin/order-groups')
        commit('setOrderGroups', data)
    },
    async getOpenOrders({commit}) {
        const {data} = await axios.get('/api/admin/open-orders')
        commit('setOpenOrders', data)
    },
}

