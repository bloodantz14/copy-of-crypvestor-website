import axios from "axios";

// state
export const state = {
  all: []
}

// getters
export const getters = {
  all: state => state.all
}

// mutations
export const mutations = {
    setClients(state, clients) {
        state.all = clients
    }
}

// actions
export const actions = {
    async getClients({commit}) {
        const {data} = await axios.get('/api/admin/open-orders')
        commit('setOpenOrders', data)
    },
}

