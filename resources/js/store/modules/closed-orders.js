import axios from "axios";

// state
export const state = {
    all: [],
    all_client: []
}

// getters
export const getters = {
    all: state => state.all,
    all_client: state => state.all_client,
}

// mutations
export const mutations = {
    setClosedOrders(state, orders) {
        state.all = orders
    },
    setClosedOrdersClient(state, orders) {
        state.all_client = orders
    }
}

// actions
export const actions = {
    async getClosedOrders({commit}) {
        const {data} = await axios.get('/api/admin/closed-orders')
        commit('setClosedOrders', data)
    },
    async getClosedOrdersClient({commit}) {
        const {data} = await axios.get('/api/dashboard/closed-orders-client')
        commit('setClosedOrdersClient', data)
    },
}

