import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// import { } from '@fortawesome/free-regular-svg-icons'

import {
    faUser,
    faLock,
    faSignOutAlt,
    faCog,
    faCoins,
    faKey,
    faSpinner,
    faTrash,
    faUsersCog,
    faCircle,
    faRobot,
    faClipboardList,
    faChevronDown, faStream, faSeedling, faUsers, faDollarSign, faWallet, faRocket, faCrosshairs
} from '@fortawesome/free-solid-svg-icons'

import {
  faGithub
} from '@fortawesome/free-brands-svg-icons'
import {faQuestionCircle} from "@fortawesome/free-regular-svg-icons";

library.add(
  faUser, faLock, faSignOutAlt, faCog, faGithub, faCoins, faKey, faSpinner, faQuestionCircle, faTrash, faUsersCog,
    faCircle, faRobot, faChevronDown, faStream, faSeedling, faUsers, faDollarSign, faClipboardList, faWallet, faRocket, faCrosshairs, faUsers
)

Vue.component('Fa', FontAwesomeIcon)
