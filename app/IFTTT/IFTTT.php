<?php


namespace App\IFTTT;

use GuzzleHttp\Client;
use Illuminate\Support\Arr;
use Exception;

class IFTTT extends Client
{
    /**
     * @var string
     */
    protected $keys;

    /**
     * GuzzleIFTTTMakerHTTPClient constructor.
     */
    public function __construct(array $options = [])
    {
        parent::__construct(Arr::get($options, 'options', []));

        $this->keys = config('ifttt.keys');
    }

    /**
     * @throws \Exception
     */
    protected function checkKeys($only_key_keys = [])
    {
        // you can pass the keys you want to use in the array on config php.
        if (!empty($only_key_keys)) {
            foreach ($this->keys as $index => $key) {
                if (empty($key) && !in_array($index, $only_key_keys)) {
                    unset($this->keys[$index]);
                }
            }
        } else {
            foreach ($this->keys as $index => $key) {
                if (empty($key)) {
                    unset($this->keys[$index]);
                }
            }
        }
        if (empty($this->keys)) {
            throw new Exception('Missing IFTTT key');
        }
    }

    /**
     * @param $eventName
     * @param $key
     * @return string
     */
    protected function getURI($eventName, $key)
    {
        return "https://maker.ifttt.com/trigger/{$eventName}/with/key/{$key}";
    }

    /**
     * @param $eventName
     * @param array $payload
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public function publishEvent($eventName, array $payload = [], $only_key_keys = []): array
    {
        $this->checkKeys($only_key_keys);
        $response = [];
        foreach ($this->keys as $key) {
            $response[] = $this->post($this->getURI($eventName, $key), [
                'json' => $payload
            ]);
        }
        return $response;
    }
}
