<?php

namespace App\Console\Commands;

use App\Helpers\BinanceHelper;
use App\Models\BinanceAccount;
use App\Models\BinanceAccountTransactions;
use App\PackageOverrides\BinanceOverride;
use Binance\API;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Lin\Binance\Binance;
use Lin\Binance\BinanceDelivery;

class TradingLogAccountsData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trading:log-accounts-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Logging balances every 1 min each account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (!config('binance.trading_enabled')) {
            $this->info("Trading is disabled.");
            return;
        }

        $last_account_logged = Cache::get('last_balance_logged');
        if ($last_account_logged) {
            $last_account_logged = BinanceAccount::query()
                ->where('active', 1)
                ->where('id', '>', $last_account_logged)->first();
        }

        if (!$last_account_logged) {
            $last_account_logged = BinanceAccount::query()
                ->where('active', 1)
                ->first();
        }

        $binance = new BinanceOverride($last_account_logged->getRawOriginal('public_key'), $last_account_logged->getRawOriginal('secret_key'));
        try {
            $binance->useServerTime();
            $prices = $binance->prices();
            $balances = $binance->balances();
            $usdt_total = BinanceHelper::getUsdtTotal($balances, $prices);
            $usdt_available = BinanceHelper::getUsdtAvailable($balances);

            $last_account_logged->balancesLog()
                ->create([
                    'total' => floatval($usdt_total),
                    'usdt' => floatval($usdt_available),
                    //'raw' => is_array($balances) ? json_encode($balances) : $balances
                ]);

            Cache::put('last_balance_logged', $last_account_logged->id);

        } catch (\Exception $e) {
            // maybe log to ifttt
            dd($e);
        }

        $this->info('Done!');
    }
}
