<?php

namespace App\Console\Commands;

use App\Helpers\AppHelper;
use App\Jobs\MarketSell;
use App\Jobs\ProcessOpenOrder;
use App\Models\Order;
use App\Models\Price;
use App\PackageOverrides\BinanceOverride;
use Illuminate\Console\Command;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class TradingProcessOpenOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trading:process-open-orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var Price
     */
    protected $price_collection;

    /**
     * @var Order
     */
    protected Order $open_order;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (!config('binance.trading_enabled')) {
            $this->info("Trading is disabled.");
            return;
        }

        try {
            $this->info('Processing Open Orders');
            $open_orders = Order::query()
                ->where('side', 'BUY')
                ->whereNull('related_order_id')
                ->get();

            $this->price_collection = Price::query()->latest('id')->first();

            foreach ($open_orders as $open_order) {
                $this->open_order = $open_order;
                $this->_processOrder();
            }
        } catch (\Exception $e) {

            $message = 'Trying to dispatch open orders: ' . $e->getMessage() . ' | Line: ' . $e->getFile() . '::' . $e->getLine();
            Log::emergency($message);

            if (!Cache::has('notification-processing-open-orders')) {
                AppHelper::notifyIFTT('Crypvestor', $message);
                Cache::add('notification-processing-open-orders', true, 60 * 30); // half an hour
            }

        }
    }


    private function _processOrder($attempt = 1)
    {
        if (now()->subMinute() > $this->price_collection->created_at) {
            if ($attempt > 2 ) {
                $this->logError(__LINE__);
                return;
            }
            Artisan::call('trading:log-prices');
            sleep(2);
            $this->price_collection = Price::query()->latest('id')->first();
            $attempt++;
            $this->_processOrder($attempt);
        }

        $symbol = $this->open_order->symbol;
        $price = $this->price_collection->prices[$symbol] ?? false;
        if (!$price) {
            $this->logError(__LINE__);
            return;
        }

        if ($this->open_order->stop_loss_price && $this->open_order->stop_loss_price >= $price) {
            MarketSell::dispatch($this->open_order)->onQueue('high');
        }

        elseif ($this->open_order->take_profit_price && $this->open_order->take_profit_price <= $price) {
            MarketSell::dispatch($this->open_order)->onQueue('high');
        }
    }

    /**
     * @param $line
     * @throws BindingResolutionException
     */
    public function logError($line)
    {
        $message = 'Cannot process a Open Order in Job (LINE: '.$line.'): Preorder ID: ' . $this->open_order->id;
        Log::emergency($message);

        if (!Cache::has('notification-job-pre-order')) {
            AppHelper::notifyIFTT('Crypvestor', $message);
            Cache::add('notification-job-pre-order', true, 60 * 30); // half an hour
        }
    }
}
