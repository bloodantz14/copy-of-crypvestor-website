<?php

namespace App\Console\Commands;

use App\Helpers\AppHelper;
use App\Models\FuturePrice;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Lin\Binance\BinanceDelivery;
use Lin\Binance\BinanceFuture;

class FuturesBotOpener extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'futures:bot-opener {--indicator=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatic Futures Trading Bot to open positions';

    /**
     * @var
     */
    private $indicator;

    /**
     * @var BinanceFuture
     */
    public BinanceFuture $api;

    /**
     * Symbol
     * @var string
     */
    public string $symbol;

    /**
     * Stable coin
     * @var string
     */
    public string $stable_coin = 'USDT';

    /**
     * Exchange Info
     * @var array
     */
    public array $exchange_info = [];

    /**
     * Query History
     * @var array
     */
    public array $history = [];

    /**
     * Filters
     * @var array
     */
    public array $filters = [];

    /**
     * Create a new command instance.
     *
     * @return void
     * @throws \Exception
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Exception
     */
    public function handle()
    {
        if (!config('binance.futures_enabled')) {
            $this->info("Buy trading is disabled.");
            return;
        }

        $this->api = new BinanceFuture(
            config('binance.default_api_key'),
            config('binance.default_api_secret')
        );

        $options = $this->options();

        $orders = $this->api->user()->getPositionRisk();
        $orders_count = collect($orders)->where('entryPrice', '>',  0)->count();
        if ($orders_count > 0) {
            $this->info("There are an open position right now, abort mission.");
            return;
        }

        if ( isset($options['indicator']) ) {
            $class = 'App\Trading\Futures\\'.ucfirst($options['indicator']);

            if (!class_exists($class)) {
                throw new \Exception("Could not find indicator $class");
            }
            $this->indicator = new $class($this);
        }


        $balance = $this->api->user()->getBalance();
        $balance_usdt = collect($balance)->where('asset', 'USDT')->first()['availableBalance'];
        if ($balance_usdt < 10) {
            $this->info("No enough balance to play.");
            return;
        }


        $this->history['last_24hs'] = FuturePrice::query()->where('created_at', '>=', Carbon::now()->subDay())->orderBy('created_at')->first();
        $this->history['last_12hs'] = FuturePrice::query()->where('created_at', '>=', Carbon::now()->subHours(12))->orderBy('created_at')->first();
        $this->history['last_6hs'] = FuturePrice::query()->where('created_at', '>=', Carbon::now()->subHours(6))->orderBy('created_at')->first();
        $this->history['last_3hs'] = FuturePrice::query()->where('created_at', '>=', Carbon::now()->subHours(3))->orderBy('created_at')->first();
        $this->history['last_2hs'] = FuturePrice::query()->where('created_at', '>=', Carbon::now()->subHours(2))->orderBy('created_at')->first();
        $this->history['last_1hs'] = FuturePrice::query()->where('created_at', '>=', Carbon::now()->subHour())->orderBy('created_at')->first();
        $this->history['last_30m'] = FuturePrice::query()->where('created_at', '>=', Carbon::now()->subMinutes(30))->orderBy('created_at')->first();
        $this->history['last_15m'] = FuturePrice::query()->where('created_at', '>=', Carbon::now()->subMinutes(15))->orderBy('created_at')->first();
        $this->history['last_10m'] = FuturePrice::query()->where('created_at', '>=', Carbon::now()->subMinutes(10))->orderBy('created_at')->first();
        $this->history['last_5m'] = FuturePrice::query()->where('created_at', '>=', Carbon::now()->subMinutes(5))->orderBy('created_at')->first();
        $this->history['last_3m'] = FuturePrice::query()->where('created_at', '>=', Carbon::now()->subMinutes(3))->orderBy('created_at')->first();
        $this->history['last_1m'] = FuturePrice::query()->where('created_at', '>=', Carbon::now()->subMinute())->orderBy('created_at')->first();

        $tickers = $this->api->market()->getTickerPrice();
        Log::info('BOT Running');
        foreach ($tickers as $ticker) {

            $this->symbol = $ticker['symbol'];

            if (
                $this->indicator
                && $this->indicator->shouldBuy($ticker)
            ) {
                //AppHelper::notifyIFTT('TEST BUY', $this->symbol);
                //dd('Should Buy ' . $this->symbol);
                $this->info("Buying Futures.");

                $this->setExchangeInfo();
                $this->setFilters();

                $last_price = $ticker['price'];

                $qty = $balance_usdt / $last_price;
                if ((float) $this->filters['LOT_SIZE']['stepSize'] > 0) {
                    $qty = $this->roundStep($qty, (float) $this->filters['LOT_SIZE']['stepSize']);
                }

                $this->api->trade()->postLeverage([
                    'symbol' => $ticker['symbol'],
                    'leverage'=> 1,
                ]);

                $order = $this->api->trade()->postOrder([
                    'symbol' => $ticker['symbol'],
                    'side'=>'SELL',
                    'type'=>'MARKET',
                    'quantity'=> $qty,
                ]);

                $link = "https://www.binance.com/en/futures/" . $this->symbol;
                AppHelper::notifyIFTT('Futures Opening', json_encode($order), $link);
            }
        }
    }

    /**
     *
     * @throws \Exception
     */
    public function setFilters()
    {
        $this->filters = [];
        foreach ($this->exchange_info['symbols'] as $filters) {
            if ($filters['symbol'] ===  $this->symbol) {
                $this->filters = array_column($filters['filters'], null, 'filterType');
                if ($this->filters === null) {
                    throw new \Exception('Could not find the trade filters');
                }
                break;
            }
        }
    }

    /**
     *
     * @throws \Exception
     */
    public function setExchangeInfo()
    {
        $this->exchange_info = $this->api->market()->getExchangeInfo();
    }

    public function roundStep($qty, $stepSize = 0.1): float
    {
        $precision = strlen(substr(strrchr(rtrim($stepSize, '0'), '.'), 1));
        return round((($qty / $stepSize) | 0) * $stepSize, $precision);
    }
}
