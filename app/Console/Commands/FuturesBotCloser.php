<?php

namespace App\Console\Commands;

use App\Helpers\AppHelper;
use App\Models\FuturePrice;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;
use Lin\Binance\BinanceDelivery;
use Lin\Binance\BinanceFuture;

class FuturesBotCloser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'futures:bot-closer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatic Futures Trading Bot to close positions';


    /**
     * @var BinanceFuture
     */
    public BinanceFuture $api;

    /**
     * Symbol
     * @var string
     */
    public string $symbol;

    /**
     * Stable coin
     * @var string
     */
    public string $stable_coin = 'USDT';

    /**
     * Exchange Info
     * @var array
     */
    public array $exchange_info = [];


    /**
     * Filters
     * @var array
     */
    public array $filters = [];


    /**
     * Roe
     * @var float
     */
    public float $roe;


    /**
     * PNL
     * @var float
     */
    public float $pnl;

    /**
     * Create a new command instance.
     *
     * @return void
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws Exception
     */
    public function handle()
    {
        if (!config('binance.futures_enabled')) {
            $this->info("Buy trading is disabled.");
            return;
        }

        $this->api = new BinanceFuture(
            config('binance.default_api_key'),
            config('binance.default_api_secret')
        );


        $orders = $this->api->user()->getPositionRisk();
        $orders = collect($orders)->where('entryPrice', '>',  0);

        foreach ($orders as $order) {
            //ROE% =Unrealized PNL in USDT / side * (exit price / entry price - 1) / IMR
            $this->symbol = $order['symbol'];
            $imr = 1 / $order['leverage'];
            $side = $order['notional'] > 0 ? 1 : -1;
            $this->roe = (1 - ($order['entryPrice'] / $order['markPrice']) ) / $imr * 100 * $side;
            $this->pnl = $order['unRealizedProfit'];
            // another ways...
            //$roe = $order['unRealizedProfit'] / ($order['positionAmt'] * $order['markPrice'] * $imr * 0.01);
            //$roe = $order['unRealizedProfit'] / ($order['notional'] * $order['entryPrice'] * $imr);
            $this->info("Profit: \${$this->pnl} {$this->roe}% | Symbol: {$order['symbol']}");
            if ($this->roe > 1) {
                $this->close($order);
            }
        }
    }

    /**
     * @throws Exception
     */
    public function close($order)
    {
        $this->info("Closing futures.");

        $this->setExchangeInfo();
        $symbols = collect($this->exchange_info['symbols']);
        $info = $symbols->where('symbol', $this->symbol)->first();
        //$this->setFilters($info);

        $side =  $order['notional'] > 0 ? 'SELL' : 'BUY';
        $type =  $order['notional'] > 0 ? 'STOP_MARKET' : 'TAKE_PROFIT_MARKET';
        $stop_price = floatval(round($order['markPrice'], $info['pricePrecision']));

        $order = $this->api->trade()->postOrder([
            'symbol' => $this->symbol,
            'closePosition' => 'true',
            'side' => $side,
            'type' => $type,
            'stopPrice' => $stop_price,
        ]);

        $link = "https://www.binance.com/en/futures/" . $order['symbol'];
        $roe_format = number_format($this->roe, 2);
        AppHelper::notifyIFTT("Futures Closing - {$roe_format}%", json_encode($order), $link);
    }

    /**
     *
     * @throws Exception
     */
    public function setExchangeInfo()
    {
        $this->exchange_info = $this->api->market()->getExchangeInfo();
    }

    /**
     * @param $info
     * @throws Exception
     */
    public function setFilters($info)
    {
        $this->filters = [];
        $this->filters = array_column($info['filters'], null, 'filterType');
        if ($this->filters === null) {
            throw new Exception('Could not find the trade filters');
        }
    }
}
