<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TradingCleaner extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trading:cleaner {--queue-clear}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleaning things';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Cleaning...');
        try {
            Artisan::call('cache:clear');
            $this->info('Cache Cleared');
            Artisan::call('config:cache');
            $this->info('Config Cached');
            if ($this->option('queue-clear')) {
                $clean = $this->confirm("Do you want to Clean the Queues?.", true);
                if ($clean) {
                    Artisan::call('queue:clear --force');
                    $this->info('Queues cleaned');
                }
            }
            Artisan::call('queue:restart');
            $this->info('Queues restarted');

            $reread = shell_exec('supervisorctl reread');
            $this->info($reread);
            $update = shell_exec('supervisorctl update all');
            $this->info($update);
            $restart = shell_exec('supervisorctl restart all');
            $this->info($restart);
            //dump($reread, $update, $restart);
            $this->info('Supervisor reloaded?, who knows.');
        } catch (\Exception $e) {
            Log::error('Error: ' . $e->getMessage());
            $this->info('Error: ' . $e->getMessage());
        }
        Log::info('Processes Restarted');
        $this->info('Processes Restarted');
    }
}
