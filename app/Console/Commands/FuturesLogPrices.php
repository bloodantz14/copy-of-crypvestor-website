<?php

namespace App\Console\Commands;

use App\Helpers\AppHelper;
use App\Models\FuturePrice;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Lin\Binance\BinanceFuture;
use Illuminate\Support\Facades\DB;

class FuturesLogPrices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'futures:log-prices {--restart}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Log future prices on realtime.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        if (!config('binance.futures_enabled')) {
            $this->info("Buy trading is disabled.");
            return;
        }

        try {
            $this->info('Logging prices...');

            $api = new BinanceFuture(
                config('binance.default_api_key'),
                config('binance.default_api_secret')
            );


            $prices_array = [];
            if ($this->option('restart')) {
                $exchange_info = $api->market()->getExchangeInfo();
                $symbols = collect($exchange_info['symbols'])->keyBy('symbol')->keys();

                FuturePrice::query()->truncate();
                foreach ($symbols as $key => $symbol) {
                    //if ($key > 1) { break; }

                    $klines = $api->market()->getKlines(['symbol' => $symbol, 'interval' => '1m', 'limit' => 1440]);
                    //$c =collect($klines);
                    foreach ($klines as $kline) {
                        $prices_array[$kline[6]][] = [ // 6 close time
                            'symbol' => $symbol,
                            'price' => $kline[4]
                        ];
                    }
                }
                foreach ($prices_array as $time => $prices) {
                    $carbon_time = Carbon::createFromTimestamp($time / 1000);
                    FuturePrice::query()->create([
                        'prices' => json_encode($prices),
                        'created_at' => $carbon_time->toDateTimeString(),
                        'updated_at' => $carbon_time->toDateTimeString(),
                    ]);
                }
            }

            $market = $api->market();
            $prices = $market->getTickerPrice();
            $prices = collect($prices)->map(function ($price) { unset($price['time']); return $price; })->toJson();
            FuturePrice::query()->create(['prices' => $prices]);

            //dd($market->getResponseHeaders());
        } catch (\Exception $e) {

            $message = 'Trying to log Prices: ' . $e->getMessage() . ' | Line: ' . $e->getFile() . '::' . $e->getLine();
            Log::emergency($message);

            if (!Cache::has('notification-log-prices')) {
                AppHelper::notifyIFTT('Crypvestor', $message);
                Cache::add('notification-log-prices', true, 60 * 30); // half an hour
            }
        }
    }
}
