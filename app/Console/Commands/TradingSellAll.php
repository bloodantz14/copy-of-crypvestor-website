<?php

namespace App\Console\Commands;

use App\Models\BinanceAccount;
use App\Trading\Helper;
use Binance\API as BinanceApi;
use Illuminate\Console\Command;

class TradingSellAll extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     *
     * currency_in: coin to be traded
     * currency_out: coin of rescue
     * --check option - use this to just loop through all of them and display the stats without selling..
     */
    protected $signature = 'trading:sell-all {binance_account=ALL} {--check}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sell all your binance coins at market value';

    /**
     * @var BinanceApi
     */
    private $api;
    private $currency_out;
    private $binance_account;
    private $check;
    private $filters = [];

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (!config('binance.trading_enabled')) {
            $this->info("Trading is disabled.");
            return;
        }

        $this->currency_out = "USDT";
        $this->check = $this->option('check');
        $this->binance_account = $this->argument('binance_account');
        if ($this->binance_account == 'ALL') {
            die('nah');
        }
        $binance_account = BinanceAccount::query()->find($this->binance_account);
        $this->api = new BinanceApi($binance_account->getRawOriginal('public_key'), $binance_account->getRawOriginal('secret_key'));
        $this->api->useServerTime();

        $info = $this->api->exchangeInfo();
        foreach ($info['symbols'] as $filters) {
            $this->filters[$filters['symbol']] = array_column($filters['filters'], null, 'filterType');
        }

        $this->start();
    }

    private function start()
    {
        $balances = $this->api->balances();
        $prices = $this->api->prices();
        foreach ($balances as $coin => $balance) {
            if (!($balance['available'] > 0)) {
                continue;
            }
            $pair = $coin.$this->currency_out;
            $price = $prices[$pair] ?? null;
            if ($price === null) {
                if (!$this->check) {
                    $this->info("Couldn't find price for pair $pair");
                }
                continue;
            }
            if (!isset($this->filters[$pair])) {
                if (!$this->check) {
                    $this->info("Couldn't find filters for coin $pair");
                }
                continue;
            }
            $value = $balance['available'] * $price;
            $qty = Helper::getBinanceQty($balance['available'], $this->filters[$pair]);
            if ($qty === false) {
                if (!$this->check) {
                    $this->info("$pair - qty:$qty is below minimum");
                }
                continue;
            }
            if ($value < 10.01) {
                if (!$this->check) {
                    $this->info("$pair - is below $10");
                }
                continue;
            }
            $bought = collect($this->api->orders($pair))->where('side', 'BUY')->last();
            if ($bought) {
                $ago = (time() - ($bought['time'] / 1000)) / 60;
                $postfix = 'mins';
                if ($ago > 120) {
                    $ago = $ago / 60;
                    $postfix = 'hours';
                    if ($ago > 48) {
                        $ago = $ago / 24;
                        $postfix = 'days';
                    }
                }
                $percent = (($value - $bought["cummulativeQuoteQty"]) / $bought["cummulativeQuoteQty"]) * 100;
                dump([
                    'pair' => $pair,
                    'percent_change' => number_format($percent, 2, '.', ',') . '%',
                    'value now' => number_format($value, 2, '.', ','),
                    'bought_value' => $bought["cummulativeQuoteQty"],
                    'time since buy' => number_format($ago, 1). ' '.$postfix,
                ]);
            } else {
                dump('Something happened here:' . $pair);
            }
            if (!$this->check) {
                $sell = $this->confirm("Do you want to sell.", true);
                if ($sell) {
                    $order = $this->api->marketSell($pair, $qty);
                    $this->info("Order placed. Status: {$order['status']}");
                }
            }
        }
    }

}
