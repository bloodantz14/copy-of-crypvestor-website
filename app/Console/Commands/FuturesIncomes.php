<?php

namespace App\Console\Commands;

use App\Helpers\AppHelper;
use App\Models\FuturePrice;
use App\Models\FuturesIncome;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Lin\Binance\BinanceDelivery;
use Lin\Binance\BinanceFuture;

class FuturesIncomes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'futures:incomes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate Profit based on orders';


    /**
     * @var BinanceFuture
     */
    public BinanceFuture $api;

    /**
     * Create a new command instance.
     *
     * @return void
     * @throws \Exception
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Exception
     */
    public function handle()
    {
        try {
            if (!config('binance.futures_enabled')) {
                $this->info("Futures is disabled.");
                return;
            }

            $this->api = new BinanceFuture(
                config('binance.default_api_key'),
                config('binance.default_api_secret')
            );

            $start_time = Carbon::now()->subDay()->timestamp;
            $incomes = $this->api->user()->getIncome(['limit' => 500, 'startTime' => $start_time * 1000]);
            foreach ($incomes as $income) {
                FuturesIncome::query()->updateOrCreate(
                    [
                        "income_type" => $income["incomeType"],
                        "tran_id" => $income["tranId"],
                        "trade_id" => $income["tradeId"]
                    ],
                    [
                        "symbol" => $income["symbol"] ?? "",
                        "income_type" => $income["incomeType"] ?? "",
                        "income" => $income["income"] ?? "",
                        "asset" => $income["asset"] ?? "",
                        "time" => Carbon::createFromTimestamp(intval($income["time"] / 1000))->toDateTimeString(),
                        "info" => $income["info"] ?? "",
                        "tran_id" => $income["tranId"] ?? "",
                        "trade_id" => $income["tradeId"] ?? "",
                    ]
                );
            }

            $this->info('Futures Income Logged');
        }

        catch (\Exception $e) {
            Log::critical('Futures Income: ' . $e->getMessage());
        }


    }

}
