<?php

namespace App\Console\Commands;

use App\Models\FuturePrice;
use App\Models\Price;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TradingDatabaseRoutines extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trading:database-routines';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Purging Database Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Cleaning db...');
        try {
            Price::query()->where('created_at', '<', Carbon::now()->subDays(2))->delete();
            FuturePrice::query()->where('created_at', '<', Carbon::now()->subDays(2))->delete();
        } catch (\Exception $e) {
            Log::error('Error: ' . $e->getMessage());
            $this->info('Error: ' . $e->getMessage());
        }
        Log::info('DB Cleaned');
        $this->info('  DB Cleaned');
    }
}
