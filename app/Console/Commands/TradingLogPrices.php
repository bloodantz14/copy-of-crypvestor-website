<?php

namespace App\Console\Commands;

use App\Exceptions\CriticalException;
use App\Helpers\AppHelper;
use App\Helpers\BinanceHelper;
use App\Models\Price;
use Illuminate\Console\Command;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use mysql_xdevapi\Exception;

class TradingLogPrices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trading:log-prices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Log prices on realtime.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        if (!config('binance.trading_enabled')) {
            $this->info("Trading is disabled.");
            return;
        }

        try {
            $this->info('Logging prices...');
            $prices = BinanceHelper::getPrices();
            Price::query()->create(['prices' => $prices]);
        } catch (\Exception $e) {

            $message = 'Trying to log Prices: ' . $e->getMessage() . ' | Line: ' . $e->getFile() . '::' . $e->getLine();
            Log::emergency($message);

            if (!Cache::has('notification-log-prices')) {
                AppHelper::notifyIFTT('Crypvestor', $message);
                Cache::add('notification-log-prices', true, 60 * 30); // half an hour
            }
        }
    }
}
