<?php

namespace App\Console\Commands;

use App\Helpers\AppHelper;
use App\Jobs\MarketBuy;
use App\Jobs\MarketSell;
use App\Jobs\ProcessOpenOrder;
use App\Jobs\ProcessPreOrder;
use App\Models\Order;
use App\Models\PreOrder;
use App\Models\Price;
use App\Models\Rule;
use App\PackageOverrides\BinanceOverride;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class TradingProcessPreOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trading:process-pre-orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dispatch Pre Orders';

    /**
     * @var Price
     */
    protected $price_collection;

    /**
     * @var PreOrder
     */
    protected PreOrder $pre_order;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (!config('binance.trading_enabled')) {
            $this->info("Trading is disabled.");
            return;
        }

        try {
            $this->info('Processing Pre Orders');
            $pre_orders = PreOrder::query()
                ->where('status', 'queued')
                ->orWhereNull('status')
                ->get();

            $this->price_collection = Price::query()->latest('id')->first();

            foreach ($pre_orders as $pre_order) {
                $this->pre_order = $pre_order;
                $this->_processPreOrder();
            }
        } catch (\Exception $e) {

            $message = 'Trying to dispatch pre orders: ' . $e->getMessage() . ' | Line: ' . $e->getFile() . '::' . $e->getLine();
            Log::emergency($message);

            if (!Cache::has('notification-processing-pre-orders')) {
                AppHelper::notifyIFTT('Crypvestor', $message);
                Cache::add('notification-processing-pre-orders', true, 60 * 30); // half an hour
            }
        }
    }

    private function _processPreOrder($attempt = 1)
    {
        if (now()->subMinute() > $this->price_collection->created_at) {
            if ($attempt > 2 ) {
                $this->logError(__LINE__);
                return;
            }
            Artisan::call('trading:log-prices');
            sleep(2);
            $this->price_collection = Price::query()->latest('id')->first();
            $attempt++;
            $this->_processPreOrder($attempt);
        }

        $symbol = $this->pre_order->symbol;
        $price = $this->price_collection->prices[$symbol] ?? false;
        if (!$price) {
            $this->logError(__LINE__);
            return;
        }

        switch ($this->pre_order->rule_id) {
            case Rule::UNDER:
                if (empty($this->pre_order->trigger_price)) {
                    $this->logError(__LINE__);
                    throw new \Exception('Trigger price should be not empty');
                }
                // under with price
                if ($this->pre_order->trigger_price > $price) {
                    $this->pre_order->update(['status' => 'processing']);
                    $this->info('Dispatching Market Buy on High queue: ' . $this->pre_order->symbol);
                    MarketBuy::dispatch($this->pre_order)->onQueue('high');
                }
                break;

            case Rule::OVER:
                if (empty($this->pre_order->trigger_price)) {
                    $this->logError(__LINE__);
                    throw new \Exception('Trigger price should be not empty');
                }
                // under with price
                if ($this->pre_order->trigger_price < $price) {
                    $this->pre_order->update(['status' => 'processing']);
                    $this->info('Dispatching Market Buy on High queue: ' . $this->pre_order->symbol);
                    MarketBuy::dispatch($this->pre_order)->onQueue('high');
                }
                break;

            case Rule::BUY_NOW:
                $this->pre_order->update(['status' => 'processing']);
                $this->info('Dispatching Market Buy on High queue: ' . $this->pre_order->symbol);
                MarketBuy::dispatch($this->pre_order)->onQueue('high');
                break;
        }
    }

    /**
     * @param $line
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function logError($line)
    {
        $message = 'Cannot process a Pre Order in Job (LINE: '.$line.'): Preorder ID: ' . $this->pre_order->id;
        Log::emergency($message);

        $this->pre_order->update([
            'status' => 'error',
            'details' => $message
        ]);

        if (!Cache::has('notification-job-pre-order')) {
            AppHelper::notifyIFTT('Crypvestor', $message);
            Cache::add('notification-job-pre-order', true, 60 * 30); // half an hour
        }
    }
}
