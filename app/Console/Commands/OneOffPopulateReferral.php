<?php

namespace App\Console\Commands;

use App\Helpers\AppHelper;
use App\Models\User;
use Illuminate\Console\Command;

class OneOffPopulateReferral extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'oneoff:referral';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::query()->get();
        foreach ($users as $user) {
            if (!$user->referral_id_owner) {
                $user->update(['referral_id_owner' => AppHelper::secureRandomString(10)]);
            }
        }
        $this->info('Done : )');
    }
}
