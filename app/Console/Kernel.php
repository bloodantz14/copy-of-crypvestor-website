<?php

namespace App\Console;

use App\Console\Commands\FuturesBotCloser;
use App\Console\Commands\FuturesBotOpener;
use App\Console\Commands\FuturesLogPrices;
use App\Console\Commands\TradingDatabaseRoutines;
use App\Console\Commands\TradingLogAccountsData;
use App\Console\Commands\TradingLogPrices;
use App\Console\Commands\TradingProcessOpenOrders;
use App\Console\Commands\TradingSellAll;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        TradingLogAccountsData::class,
        TradingSellAll::class,
        TradingProcessOpenOrders::class,
        TradingLogPrices::class,
        TradingDatabaseRoutines::class,

        FuturesLogPrices::class,
        FuturesBotCloser::class,
        FuturesBotOpener::class
    ];

    /**
     * php artisan queue:work --queue=high,default
     * php artisan short-schedule:run
     *
     * Short Scheculing
     * @param \Spatie\ShortSchedule\ShortSchedule $shortSchedule
     */
    protected function shortSchedule(\Spatie\ShortSchedule\ShortSchedule $shortSchedule)
    {
        /*
         * Log prices regularly
         */
        $shortSchedule->command('trading:log-prices')
            ->everySeconds(5)
            ->withoutOverlapping();

        /*
         * Process open orders detecting if we have to create a job to sell
         */
        $shortSchedule->command('trading:process-open-orders')
            ->everySeconds(5)
            ->when(function () {
                return true; // add logic here.
            })
            ->withoutOverlapping();


        /*
         * Process pre orders detecting if we have to create a job to buy
         */
        $shortSchedule->command('trading:process-pre-orders')
            ->everySeconds(5)
            ->when(function () {
                return true; // add logic here.
            })
            ->withoutOverlapping();

        /*
         * Futures Logging prices
         */
        $shortSchedule->command('futures:log-prices')
            ->everySeconds(5)
            ->withoutOverlapping();

        /*
         * Futures Logging prices
         */
        $shortSchedule->command('futures:bot-opener --indicator=PriceChange')
            ->everySeconds(10)
            ->withoutOverlapping();

        /*
         * Futures Logging prices
         */
        $shortSchedule->command('futures:bot-closer')
            ->everySeconds(10)
            ->withoutOverlapping();

    }


    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        /*
         * Log Data for binance accounts, pulling balances and transactions
         */
        $schedule->command('trading:log-accounts-data')->everyTwoMinutes();

        /*
         * Futures Incomes
         */
//        $schedule->command('futures:incomes')
//            ->hourly()
//            ->withoutOverlapping();

        /*
         * Clean Database
         */
        $schedule->command('trading:database-routines')->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
