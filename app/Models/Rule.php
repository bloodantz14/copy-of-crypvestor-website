<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rule extends Model
{
    const UNDER = 1;
    const OVER = 2;
    const BUY_NOW = 3;

}
