<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BinanceAccountTransactions extends Model
{
    use HasFactory;

    protected $fillable = [
        'tx_id',
        'coin',
        'amount',
        'usdt_amount',
        'address',
        'tx_time',
        'status',
        'type'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function binanceAccount(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(BinanceAccount::class);
    }
}
