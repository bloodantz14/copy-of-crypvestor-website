<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BinanceAccount
 * @package App\Models
 */
class BinanceAccount extends Model
{
    use HasFactory, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'binance_accounts';

    protected $hidden = ['initial_balances_array'];

    protected $fillable = [
        'user_id',
        'name',
        'public_key',
        'secret_key',
        'strategy_id',
        'initial_balance_total',
        'initial_balances_array',
        'note',
        'active',
        'exclude_commission'
    ];

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    /**
     * @param $value
     * @return string
     */
    public function getPublicKeyAttribute($value): string
    {
        return substr($value, -10, 10) . '**********';
    }

    /**
     * @param $value
     * @return string
     */
    public function getSecretKeyAttribute($value): string
    {
        return substr($value, -10, 10) . '**********';
    }

    /**
     * @return BelongsTo
     */
    public function strategy(): BelongsTo
    {
        return $this->belongsTo(Strategy::class);
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return HasMany
     */
    public function balancesLog(): HasMany
    {
        return $this->hasMany(BinanceAccountBalances::class);
    }

    public function transactionsLog(): HasMany
    {
        return $this->hasMany(BinanceAccountTransactions::class);
    }

    /**
     * @return HasOne
     */
    public function lastBalance()
    {
        return $this->hasOne(BinanceAccountBalances::class)->latest();
    }
}
