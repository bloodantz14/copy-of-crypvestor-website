<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PriceChange
 * @package App\Models
 */
class OrderLog extends Model
{
    protected $fillable = [
        'order_id',
        'price',
        'profit',
        'note',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

}
