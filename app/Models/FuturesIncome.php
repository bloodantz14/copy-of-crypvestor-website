<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FuturesIncome extends Model
{
    use HasFactory;

    protected $fillable = [
        "symbol",
        "income_type",
        "income",
        "asset",
        "time",
        "info",
        "tran_id",
        "trade_id",
    ];

    protected $dates = [
        'time'
    ];

}
