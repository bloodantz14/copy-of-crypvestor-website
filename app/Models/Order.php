<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Order extends Model
{
    use HasFactory;


    protected $fillable = [
        'binance_account_id',
        'order_group_id',
        'symbol',
        'order_id',
        'transact_time',
        'price',
        'orig_qty',
        'executed_qty',
        'cumulative_quote_qty',
        'status',
        'time_in_force',
        'stop_price',
        'stop_percent',
        'profit_percent',
        'profit_amount',
        'type',
        'side',
        'fills',
        'related_order_id',
        'internal_status',
        'internal_details',
        'pre_order_id',
        'billing_status_id',
    ];

    protected $appends = [
        'average_price',
        'transact_time_humans',
        'last_price',
        'stop_loss_price',
        'take_profit_price',
    ];


    public function getTakeProfitPriceAttribute()
    {
        $selling_at = null;
        if (!empty($this->profit_amount)) {
            $profit_amount = floatval($this->profit_amount);
            $purchase_price = floatval($this->average_price);
            $selling_at = ($profit_amount + $purchase_price);
        }
        if (!empty($this->profit_percent)) {
            $profit_percent = floatval($this->profit_percent);
            $purchase_price = floatval($this->average_price);
            $selling_at = ((($profit_percent / 100) * $purchase_price) + $purchase_price);
        }
        return $selling_at;
    }

    public function getStopLossPriceAttribute()
    {
        $selling_at = null;
        if (!empty($this->stop_price)) {
            $stop_price = floatval($this->stop_price);
            $purchase_price = floatval($this->average_price);
            $selling_at = ($stop_price + $purchase_price);
        }
        if (!empty($this->stop_percent)) {
            $stop_percent = floatval($this->stop_percent);
            $purchase_price = floatval($this->average_price);
            $selling_at = ((($stop_percent / 100) * $purchase_price) + $purchase_price);
        }
        return $selling_at;
    }

    public function getLastPriceAttribute()
    {
        // only for open orders
        if(empty($this->attributes['related_order_id'])) {
            $last_prices = Price::query()->latest('id')->first();
            if ($last_prices) {
                return $last_prices->prices[$this->attributes['symbol']] ?? false;;
            } else {
                return 'no prices found';
            }
        }
        return 'nah, unnecessary';
    }

    public function getTransactTimeAttribute(): Carbon
    {
        return Carbon::createFromTimestamp( intval($this->attributes['transact_time'] / 1000) );
    }

    public function getTransactTimeHumansAttribute(): string
    {
        return Carbon::createFromTimestamp( intval($this->attributes['transact_time'] / 1000) )->diffForHumans();
    }

    /**
     * @return BelongsTo
     */
    public function binanceAccount(): BelongsTo
    {
        return $this->belongsTo(BinanceAccount::class);
    }

    /**
     * @return BelongsTo
     */
    public function orderStatus(): BelongsTo
    {
        return $this->belongsTo(OrderStatus::class);
    }

    /**
     * @return BelongsTo
     */
    public function relatedOrder(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * @return BelongsTo
     */
    public function preOrder(): BelongsTo
    {
        return $this->belongsTo(PreOrder::class);
    }

    /**
     * @return BelongsTo
     */
    public function preOrderTrashed(): BelongsTo
    {
        return $this->belongsTo(PreOrder::class, 'pre_order_id')->withTrashed();
    }

    public function getAveragePriceAttribute()
    {
        if ($this->price == 0) {
            $purchase_price = json_decode($this->fills);
            $purchase_price = collect($purchase_price)->keyBy('price')->keys()->avg();
        } else {
            $purchase_price = $this->price;
        }
        return $purchase_price;
    }

    public function logs()
    {
        return $this->hasMany(OrderLog::class);
    }

    public function addLog($price = null, $profit = null, $note = null)
    {
        $this->logs()->create([
            'price' => $price,
            'profit' => $profit,
            'note' => $note
        ]);
    }
}
