<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FuturePrice extends Model
{
    protected $fillable = [
        'prices',
        'created_at',
        'updated_at',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function getPricesAttribute()
    {
        $prices = json_decode($this->attributes['prices'], true);
        $prices = collect($prices)->filter(function ($price) {
            return false !== stristr($price['symbol'], 'USDT');
        });
        return $prices->pluck('price', 'symbol');
    }

    public function getOriginalPricesAttribute()
    {
        $prices = json_decode($this->attributes['prices'], true);
        return collect($prices)->filter(function ($price) {
            return false !== stristr($price['symbol'], 'USDT');
        })->values();
    }
}
