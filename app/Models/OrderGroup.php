<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderGroup extends Model
{
    use HasFactory;

    protected $fillable = [
        'symbol',
        'size_amount',
        'size_percent',
        'rule_id',
        'trigger_price',
        'stop_loss_percent',
        'take_profit_percent',
    ];

    /**
     * @return BelongsTo
     */
    public function rule(): BelongsTo
    {
        return $this->belongsTo(Rule::class);
    }

    /**
     * @return HasManyThrough
     */
    public function binanceAccounts()
    {
        return $this->hasManyThrough(BinanceAccount::class, Order::class);
    }

    /**
     * @return HasMany
     */
    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }

    /**
     * @return HasMany
     */
    public function preOrders(): HasMany
    {
        return $this->hasMany(PreOrder::class);
    }

    /**
     * @return HasMany
     */
    public function preOrdersTrashed(): HasMany
    {
        return $this->hasMany(PreOrder::class, 'pre_order_id')->withTrashed();
    }

}
