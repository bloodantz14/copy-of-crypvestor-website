<?php


namespace App\PackageOverrides;


use Binance\API;

class BinanceOverride extends API
{
    public function __construct($api_key, $secret)
    {
        if (config('binance.use_testnet')) {
            $api_key = config('binance.testnet_api_key');
            $secret = config('binance.testnet_api_secret');
            $this->useTestnet = true;
        }
        parent::__construct($api_key, $secret);
    }


    /**
     * depositHistory - Get the deposit history for one or all assets
     *
     * @link https://binance-docs.github.io/apidocs/spot/en/#deposit-history-supporting-network-user_data
     *
     * @property int $weight 1
     *
     * @param string $asset    (optional)  An asset, e.g. BTC - or leave empty for all
     * @param array  $params   (optional)  An array of additional parameters that the API endpoint allows
     *
     * @return array containing the response
     * @throws \Exception
     */
    public function depositHistory(string $asset = null, array $params = []): array
    {
        $params["sapi"] = true;
        if (is_null($asset) === false) {
            $params['coin'] = $asset;
        }
        //$return = $this->httpRequest("v1/capital/deposit/hisrec", "GET", $params, true);
        // Wrapping in array for backwards compatibility with wapi
        $return = array(
            'depositList' => $this->httpRequest("v1/capital/deposit/hisrec", "GET", $params, true)
        );
        // Adding for backwards compatibility with wapi
        $return['success'] = 1;

        return $return;
    }

    /**
     * balances get balances for the account assets
     *
     * $balances = $api->balances($ticker);
     *
     * @param bool $priceData array of the symbols balances are required for
     * @return array with error message or array of balances
     * @throws \Exception
     */
    public function balances($priceData = false)
    {
        if (is_array($priceData) === false) {
            $priceData = false;
        }

        $account = $this->httpRequest("v3/account", "GET", [], true);

        if (is_array($account) === false) {
            return [];
        }

        if (isset($account['balances']) === false || empty($account['balances'])) {
            return [];
        }

        return $this->balanceData($account, $priceData);
    }
}
