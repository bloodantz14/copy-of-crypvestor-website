<?php


namespace App\Trading\Futures;

use App\Interfaces\FuturesTradingInterface;
use App\Console\Commands\FuturesBotOpener;
use App\Console\Commands\FuturesBotCloser;

class PriceChange implements FuturesTradingInterface
{
    /**
     * @var FuturesBotOpener|FuturesBotCloser
     */
    private $trader;

    public function __construct($trader)
    {
        $this->trader = $trader;
    }

    /**
     * @param $ticker
     * @return bool
     */
    public function shouldBuy($ticker) : bool
    {
        $symbol = $ticker['symbol'];

        if (false === strstr($symbol, 'USDT')) {
            return false;
        }

        $last_price = $ticker['price'];

        $last_24hs = $this->trader->history['last_24hs']->prices[$symbol] ?? false;
        $last_12hs = $this->trader->history['last_12hs']->prices[$symbol] ?? false;
        $last_6hs = $this->trader->history['last_6hs']->prices[$symbol] ?? false;
        $last_3hs = $this->trader->history['last_3hs']->prices[$symbol] ?? false;
        $last_1hs = $this->trader->history['last_1hs']->prices[$symbol] ?? false;
        $last_30m = $this->trader->history['last_30m']->prices[$symbol] ?? false;
        $last_15m = $this->trader->history['last_15m']->prices[$symbol] ?? false;
        $last_10m = $this->trader->history['last_10m']->prices[$symbol] ?? false;
        $last_5m = $this->trader->history['last_5m']->prices[$symbol] ?? false;
        $last_3m = $this->trader->history['last_3m']->prices[$symbol] ?? false;
        $last_1m = $this->trader->history['last_1m']->prices[$symbol] ?? false;

        $this->trader->info($symbol);
        $this->trader->info(
            '24hs: ' . $this->_getPercent($last_24hs, $last_price) . '% | ' .
            '12hs: ' . $this->_getPercent($last_12hs, $last_price) . '% | ' .
            '6hs: ' . $this->_getPercent($last_6hs, $last_price) . '% | ' .
            '1hs: ' . $this->_getPercent($last_1hs, $last_price) . '% | ' .
            '30m: ' . $this->_getPercent($last_30m, $last_price) . '% | ' .
            '15m: ' . $this->_getPercent($last_15m, $last_price) . '% | ' .
            '10m: ' . $this->_getPercent($last_10m, $last_price) . '% | ' .
            '5m: ' . $this->_getPercent($last_5m, $last_price) . '% | ' .
            '3m: ' . $this->_getPercent($last_3m, $last_price) . '% | ' .
            '1m: ' . $this->_getPercent($last_1m, $last_price) . '% | '
        );

        if (
            $this->_getPercent($last_24hs, $last_price) > 20
            && $this->_getPercent($last_1hs, $last_price) > 4
            && $this->_getPercent($last_15m, $last_price) > 2
            && $this->_getPercent($last_5m, $last_price) > 3
            && $this->_getPercent($last_3m, $last_price) > 3
            && $this->_getPercent($last_1m, $last_price) > 1
        ) {
            return true;
        }
        return false;
    }

    /**
     * @param $order
     */
    public function afterBuy($ticker, $order)
    {
        $this->trader->info('After buy, not caching due not necessary. I\'ll cache it when selling.');
    }

    /**
     * @param $orderPlaced
     */
    public function afterSell($orderPlaced)
    {
        $this->trader->info('after sell, caching');
    }

    /**
     * @param $ticker
     * @return bool
     */
    public function shouldSell($ticker) : bool
    {
        return false;
    }

    public function stableAmountToTrade() : float
    {
        //$this->trader->balances is available here;
        return env('TRADING_AMOUNT', 15);
    }

    private function _getPercent($previous, $last)
    {
        if ($previous !== false) {
            return (( $last - $previous ) / $previous ) * 100;
        }
        return false;
    }
}
