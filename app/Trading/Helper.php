<?php


namespace App\Trading;

use App\Helpers\BinanceHelper;
use App\IFTTT\IFTTT;
use App\Models\Order;
use App\Models\OrderLog;
use App\PackageOverrides\BinanceOverride;
use Binance\API;
use Binance\API as BinanceApi;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use LupeCode\phpTraderNative\Trader;
use phpDocumentor\Reflection\Types\Integer;

class Helper
{
    public static function bollinger(Collection $values)
    {
        $mid = $values->avg();

        // calculate bands bollinger
        // http://www.great-trades.com/Help/bollinger%20bands%20calculation.htm
        $low_band_split_sum = 0;
        foreach ($values as $value) {
            $low_band_split = $value - $mid;
            $low_band_split = $low_band_split * $low_band_split;
            $low_band_split_sum += $low_band_split;
        }
        $low_band = $low_band_split_sum / $values->count();
        $square_root = sqrt($low_band);
        $low = $mid - (2 * $square_root);
        $top = $mid + (2 * $square_root);

        return [
            'low' => $low,
            'mid' => $mid,
            'top' => $top,
        ];
    }

    /**
     * @param $title
     * @param $message
     * @param null $link
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public static function notification($title, $message, $link = null)
    {
        $ifttt = app()->make(IFTTT::class);
        $payload = [
            'value1' => $title,
            'value2' => $message,
        ];
        if ($link) {
            $payload['value3'] = $link;
        }
        $ifttt->publishEvent(config('ifttt.event_name'), $payload);

    }

    /**
     * @param Order $open_order
     * @param int $attempt
     * @param bool $exception
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public static function sellOrder(Order $open_order, $attempt = 0, $exception = '')
    {
        if ($attempt >= 3) {
            Helper::notification("$open_order->symbol could not be sold", '-');
            throw new \Exception("$open_order->symbol could not be sold: $exception");
        }

        $api = new BinanceApi(config('trading.binance_api_key'), config('trading.binance_secret_key'));
        $api->useServerTime();
        $exchange_info = Cache::get('exchangeInfo');
        if (!$exchange_info) {
            $exchange_info = $api->exchangeInfo();
            Cache::put('exchangeInfo', $exchange_info, now()->addMinutes(15));
        }

        // filters
        $filters = [];
        try {
            foreach ($exchange_info['symbols'] as $filters) {
                if ($filters['symbol'] ===  $open_order->symbol) {
                    $filters = array_column($filters['filters'], null, 'filterType');
                    if ($filters === null) {
                        throw new \Exception('Could not find the trade filters');
                    }
                    break;
                }
            }

            $coin = str_replace('USDT', '', $open_order->symbol);
            $balances = $api->balances();
            $coin_balance = $balances[$coin]['available'] ?? 0;
            if ((float)$filters['LOT_SIZE']['stepSize'] > 0) {
                $coin_balance = $api->roundStep($coin_balance, (float)$filters['LOT_SIZE']['stepSize']);
            }
            $order = $api->marketSell($open_order->symbol, $coin_balance);
            if ($order['status'] !== 'FILLED' && $order['status'] !== 'CANCELED') {
                //This part is untested because the orders are always filled straight away
                while ($order['status'] !== 'FILLED' && $order['status'] !== 'CANCELED') {
                    sleep(1);
                    $order = $api->orderStatus($open_order->symbol, $order['orderId']);
                }
            }
            $order['fills'] = json_encode($order['fills']);
            $order['related_order_id'] = $open_order->id;
            $new_order = Order::query()->create($order);
            $open_order->related_order_id = $new_order->id;
            $open_order->save();
            Helper::notification("$open_order->symbol sold successfully", '-');
        } catch (\Exception $e) {
            Helper::notification("Seller Error: $open_order->symbol", $e->getMessage());
            self::sellOrder($open_order, $attempt+1, $e->getMessage());
        }

    }

    /*
     * @param array $filters from api.binance.com/api/v1/exchangeInfo
     * @param float|false|mixed $qty
     * @return false|float|mixed
     */
    public static function getBinanceQty($qty, $filters)
    {
        if (isset($filters['LOT_SIZE']['stepSize'])
            &&(float) $filters['LOT_SIZE']['stepSize'] > 0) {
            $stepSize = $filters['LOT_SIZE']['stepSize'];
            $precision = strlen(substr(strrchr(rtrim($stepSize, '0'), '.'), 1));
            $qty = round((($qty / $stepSize) | 0) * $stepSize, $precision);
        }
//        if ($qty < $filters['MARKET_LOT_SIZE']['minQty']) {
//            return false;
//        }
//        if ($qty > $filters['LOT_SIZE']['maxQty']) {
//            $qty = $filters['LOT_SIZE']['maxQty'];
//        }
        return $qty;
    }

    /**
     * @param string $type
     * NOTIFY_PRICE_CHANGE
     * NOTIFY_BUY_COIN
     * NOTIFY_SELL_COIN
     * NOTIFY_COIN_NOT_SOLD_30
     * @return bool
     */
    public static function shouldNotify(string $type)
    {
        $upper = strtoupper($type);
        $check = [
            'NOTIFY_PRICE_CHANGE' => env('NOTIFY_PRICE_CHANGE', 0),
            'NOTIFY_BUY_COIN' => env('NOTIFY_BUY_COIN', 1),
            'NOTIFY_SELL_COIN' => env('NOTIFY_SELL_COIN', 1),
            'NOTIFY_COIN_NOT_SOLD_30' => env('NOTIFY_COIN_NOT_SOLD_30', 1),
        ];
        return !empty($check[$upper]);
    }

    public static function getUsdtTotal($balances, $prices)
    {
        $usdt_total = 0;
        foreach ($balances as $coin => $balance) {
            if ($coin === 'USDT') {
                $coin_total = $balance['available'] + $balance['onOrder'];
                $usdt_total = $usdt_total + $coin_total;
            } else if ($balance['available'] > 0 || $balance['onOrder'] > 0) {
                $symbol = $coin.'USDT';
                $coin_total = $balance['available'] + $balance['onOrder'];
                $current_price = $prices[$symbol] ?? 0;
                $usdt = $coin_total * $current_price;
                $usdt_total = $usdt_total + $usdt;
            }
        }
        return $usdt_total;
    }

    /**
     * Use this to increase the profit by an increasing window
     *
     * @param $api
     * @param $open_price
     * @param $symbol
     * @param $current_profit
     * @param int $sleep_time
     * @param float $increase_amount
     * @param Order $openOrder
     * @param null $console
     * @param string $msg
     * @return bool
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public static function increaseTakePrice($api, $open_price, $symbol, $current_profit, $sleep_time = 500, $increase_amount = 0.25, $openOrder = null, $console = null, $msg = '')
    {
        if ($console) {
            $console->info("$msg | $symbol");
        }
        $min_percent = $current_profit - $increase_amount;
        $max_percent = $current_profit + $increase_amount;
        if ($openOrder) {
            $openOrder->addLog(null, $current_profit, "Increasing profit by $increase_amount% increments");
        }
        while (true) {
            $purchase_time = Carbon::createFromTimestamp(intval($openOrder->transactTime/1000));
            $minutes_since_purchase = Carbon::now()->diffInMinutes($purchase_time);
            usleep($sleep_time);
            $new_price = (float) $api->price($symbol);
            $current_profit = (($new_price - $open_price) / $open_price) * 100;
            if ($current_profit > $max_percent) {
                $min_percent = $current_profit - $increase_amount;
                $max_percent = $current_profit + $increase_amount;
                $min_formatted = number_format($min_percent, 2);
                $max_formatted = number_format($max_percent, 2);
                if ($openOrder) {
                    $openOrder->addLog($new_price, $current_profit, "Increasing, min: $min_formatted% - max: $max_formatted%");
                }
                if ($console) {
                    $console->info("Increasing profit by $increase_amount%, min: $min_percent% - max: $max_percent% | {$symbol}");
                }
                continue;
            } else if ($current_profit < $min_percent) {
                if ($openOrder) {
                    $openOrder->addLog($new_price, $current_profit, "Finished increasing profit: $msg");
                }

                if ($console) {
                    $console->info("Selling with: $current_profit% profit | $symbol");
                }
                $current_profit = number_format($current_profit, 2);
                if (Helper::shouldNotify('NOTIFY_SELL_COIN')) {
                    Helper::notification("{$msg}", "Profit change: {$current_profit}% | {$symbol} | Lapse: {$minutes_since_purchase} min.");
                }
                return true;
            }
        }
    }

    public static function getBalanceTotals($balances)
    {
        $totals = [];
        foreach ($balances as $coin => $balance) {
            $totals[$coin] = $balance['available'] + $balance['onOrder'];
        }
        return $totals;
    }

    /**
     * @param $account
     * @return array
     */
    public static function getBinanceAccountsDataAPI($account)
    {
        if (!$account->active) {
            return false;
        }

        try {
            $binance = new BinanceOverride($account->getRawOriginal('public_key'), $account->getRawOriginal('secret_key'));
            $prices = $binance->prices();
            $balances = $binance->balances();
            $usdt_total = 0;
            $usdt_total_raw = 0;
            if (!empty($balances)) {
                $usdt_total_raw = BinanceHelper::getUsdtTotal($balances, $prices);
                $usdt_total = number_format($usdt_total_raw, 2);
            }
            $binance_data = [
                'binance_account_id' => $account->id,
                'prices' => $prices,
                'balances' => $balances,
                'usdt_total' => $usdt_total,
                'usdt_total_raw' => (float) $usdt_total_raw,
                'usdt_total_initial' => $account->initial_balance_total ? number_format(str_replace(',', '', $account->initial_balance_total), 2) : 0,
                'usdt_total_initial_raw' => $account->initial_balance_total ? (float) str_replace(',', '', $account->initial_balance_total) : 0
            ];
        } catch (\Exception $e) {
            $binance_data['error'] = "One of your accounts seems to have problems to interact with Binance.
                    Please check that the account name '{$account->name}' is set correctly.";
        }
        return $binance_data;
    }
}
