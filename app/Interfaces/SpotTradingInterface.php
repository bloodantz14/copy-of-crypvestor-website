<?php


namespace App\Interfaces;

/**
 * Interface Contract
 * @package App\Trading\Spot
 */
interface SpotTradingInterface
{
    public function __construct($trader);

    public function shouldBuy($ticker):bool;
    public function afterBuy($ticker, $order);

    public function shouldSell($ticker):bool;
    public function afterSell($orderPlaced);

    public function stableAmountToTrade():float;
}
