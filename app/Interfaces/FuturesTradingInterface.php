<?php


namespace App\Interfaces;

/**
 * Interface Contract
 * @package App\Trading\Futures
 */
interface FuturesTradingInterface
{
    public function __construct($trader);

    public function shouldBuy($ticker):bool;
    public function afterBuy($ticker, $order);

    public function shouldSell($ticker):bool;
    public function afterSell($orderPlacedPlaced);

    public function stableAmountToTrade():float;
}
