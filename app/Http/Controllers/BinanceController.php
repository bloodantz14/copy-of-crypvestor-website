<?php

namespace App\Http\Controllers;

use App\Helpers\BinanceHelper;
use App\Models\BinanceAccount;
use App\Models\FuturesIncome;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\Price;
use App\Models\Strategy;
use App\Models\User;
use App\PackageOverrides\BinanceOverride;
use App\Trading\Helper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;

class BinanceController extends Controller
{

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getClosedOrdersClient(Request $request): \Illuminate\Http\JsonResponse
    {
        $closed_orders = Order::query()
            ->with([
                'binanceAccount.strategy',
                'binanceAccount.user',
                'orderStatus',
                'relatedOrder.binanceAccount.user',
                'relatedOrder.binanceAccount.strategy',
                'preOrderTrashed'
            ])
            ->whereHas('relatedOrder')
            ->whereHas('binanceAccount.user', function ($query) {
                $query->where('id', Auth::user()->id);
            })
            ->where('side', 'BUY')
            ->orderByDesc('created_at')
            ->get();

        return response()->json($closed_orders, 200);
    }

    /**
     * Data for graph to represent the pnl
     * @param \Illuminate\Http\Request $request
     * @param $start_date
     * @param false $binance_account_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPnlLastMonth(Request $request, $start_date, $binance_account_id = false)
    {
        if ($binance_account_id) {
            $binance_account_id = filter_var($binance_account_id, FILTER_SANITIZE_NUMBER_INT);
        }

        $start_date = Carbon::parse($start_date);

        $closed_orders = Order::query()
            ->with([
                'binanceAccount.strategy',
                'binanceAccount.user',
                'relatedOrder.binanceAccount.user',
                'relatedOrder.binanceAccount.strategy'
            ])
            ->whereHas('relatedOrder')
            ->whereHas('binanceAccount.user', function ($query) {
                $query->where('id', Auth::user()->id);
            })
            ->whereHas('relatedOrder', function ($query) use ($start_date) {
                $query->whereDate('created_at', '>=', $start_date);
            })
            ->where('side', 'BUY');

        if ($binance_account_id) {
            $closed_orders = $closed_orders->where('binance_account_id', $binance_account_id);
        }

        $closed_orders = $closed_orders
            ->limit(500)
            ->orderBy('transact_time')
            ->get();

        $yaxis = [];
        $xaxis = [];
        $sum_pnl = 0;
        foreach ($closed_orders as $order) {
            if (
                is_null($order->relatedOrder->executed_qty)
                || is_null($order->relatedOrder->average_price)
                || is_null($order->executed_qty)
                || is_null($order->average_price)
            ) {
                continue;
            }
            $pnl = ($order->relatedOrder->executed_qty * $order->relatedOrder->average_price)
                - ($order->executed_qty * $order->average_price);
            $roe = ($order->relatedOrder->average_price - $order->average_price) / $order->average_price  * 100;
            $sum_pnl += $pnl;
            $yaxis['data'][] = number_format((float)$sum_pnl, 2, '.', '');
            $yaxis['extra'][] = [
                'sum' => number_format((float)$sum_pnl, 2),
                'pnl' => number_format((float)$pnl, 2),
                'roe' => number_format((float)$roe, 2)
            ];
            $xaxis[] = $order->relatedOrder->created_at->format('d/m H:i');
        }

        $data = [
            'series' => $yaxis,
            'datasets' => $xaxis,
        ];

        return response()->json($data, 200);
    }

    /**
     * Data for graph to represent the pnl
     * @param \Illuminate\Http\Request $request
     * @param $start_date
     * @param false $binance_account_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPnlLast30Days(Request $request)
    {
        $start_date = Carbon::now()->subMonth();

        $closed_orders = Order::query()
            ->with([
                'binanceAccount.strategy',
                'relatedOrder',
                'preOrderTrashed',
            ])
            ->whereHas('relatedOrder')
            ->whereHas('relatedOrder', function ($query) use ($start_date) {
                $query->whereDate('created_at', '>=', $start_date);
            })
            ->where('side', 'BUY');

        $binance_account_id = 7; // Account -> Binance1
        if ($binance_account_id) {
            $closed_orders = $closed_orders->where('binance_account_id', $binance_account_id);
        }

        $closed_orders = $closed_orders
            ->limit(500)
            ->orderBy('transact_time')
            ->get();

        $yaxis = [];
        $xaxis = [];
        $sum_pnl = 0;
        $cumulative_gross_profit = 0;
        foreach ($closed_orders as $order) {
            if (
                is_null($order->relatedOrder->executed_qty)
                || is_null($order->relatedOrder->average_price)
                || is_null($order->executed_qty)
                || is_null($order->average_price)
            ) {
                continue;
            }
            $pnl = ($order->relatedOrder->executed_qty * $order->relatedOrder->average_price)
                - ($order->executed_qty * $order->average_price);
            $roe = ($order->relatedOrder->average_price - $order->average_price) / $order->average_price  * 100;

            // check if an order was open with the same symbol when current one started
            $previous_orders = $closed_orders->filter(function($closed_order) use ($order) {
                if ($closed_order->symbol != $order->symbol) {
                    //return false;
                }

                if (!$closed_order->relatedOrder && $closed_order->created_at < $order->created_at) {
                    return true;
                }

                if ($closed_order->relatedOrder
                    && $closed_order->created_at < $order->created_at
                    && $closed_order->relatedOrder->created_at > $order->created_at) {
                    return true;
                }

                return false;
            });

            $previous_order = $previous_orders->last();
            $traded_capital = $order->preOrderTrashed ? $order->preOrderTrashed->size_percent ?? 100 : 100;
            if ($previous_order && $previous_order->preOrderTrashed && $previous_order->preOrderTrashed->size_percent) {
                $left_capital = 100 - $previous_order->preOrderTrashed->size_percent;
                $traded_capital = $traded_capital / 100 * $left_capital;
            }

            $gross_profit = $roe * ($traded_capital / 100);
            $cumulative_gross_profit += $gross_profit;

            $yaxis['data'][] = number_format((float)$cumulative_gross_profit, 2, '.', '');
            $yaxis['extra'][] = [
                'cumulative_gross_profit' => number_format((float)$cumulative_gross_profit, 2),
                'gross_profit' => number_format((float)$gross_profit, 2),
                'trade_profit' => number_format((float)$roe, 2)
            ];
            $xaxis[] = $order->relatedOrder->created_at->format('d/m H:i');

            $table_data[] = [
                'symbol' => $order->symbol,
                'start_date' => $order->transact_time->format('d/m H:i'),
                'end_date' => $order->relatedOrder->transact_time->format('d/m H:i'),
                'traded_capital' => $traded_capital,
                'trade_profit' => $roe,
                'gross_profit' => $gross_profit,
                'cumulative_gross_profit' => $cumulative_gross_profit,
            ];
        }

        $data = [
            'graph_data' => [
                'series' => $yaxis,
                'datasets' => $xaxis,
            ],
            'table_data' => $table_data
        ];

        return response()->json($data, 200);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function addAccount(Request $request): \Illuminate\Http\JsonResponse
    {
        $this->validate($request, [
            'public_key' => 'required|string',
            'name' => 'required|string',
            'secret_key' => 'required|string',
            'strategy_id' => 'required|integer|exists:strategies,id',
            'note' => 'nullable|string',
        ]);

        $request->merge(['user_id' => Auth::user()->id]);
        $binance_account = BinanceAccount::query()->create($request->all());

        try {
            $binance = new BinanceOverride($binance_account->getRawOriginal('public_key'), $binance_account->getRawOriginal('secret_key'));
            $binance->useServerTime();
            $prices = $binance->prices();
            $balances = $binance->balances();
            $usdt_total = BinanceHelper::getUsdtTotal($balances, $prices);
            $binance_account->update([
                'initial_balance_total' => $usdt_total,
                'initial_balances_array' => $balances,
            ]);
        } catch (\Exception $e) {
            // nothing
        }


        return response()->json('Account added successfully', 200);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateAccount(Request $request): \Illuminate\Http\JsonResponse
    {
        $this->validate($request, [
            'binance_account_id' => 'required|integer|exists:binance_accounts,id',
            'active' => 'required|boolean',
            'strategy_id' => 'required|integer|exists:strategies,id'
        ]);

        $account_belongs_to_current_user = BinanceAccount::query()
            ->where('user_id', Auth::user()->id)
            ->where('id', $request->binance_account_id)
            ->exists();

        if ($account_belongs_to_current_user || Auth::user()->isAdmin()) {
            BinanceAccount::query()
                ->where('id', $request->binance_account_id)
                ->update([
                    'active' => $request->active,
                    'strategy_id' => $request->strategy_id
                ]);

            return response()->json('Account updated successfully', 200);
        }

        else {
            return response()->json('The account specified does not belongs to you or not exists', 400);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $binance_account_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAccount(Request $request, $binance_account_id): \Illuminate\Http\JsonResponse
    {
        $account_belongs_to_current_user = BinanceAccount::query()
            ->where('user_id', Auth::user()->id)
            ->where('id', $request->binance_account_id)
            ->exists();

        if ($account_belongs_to_current_user || Auth::user()->isAdmin()) {
            BinanceAccount::query()
                ->where('id', $request->binance_account_id)
                ->delete();

            return response()->json('Account deleted successfully', 200);
        }
        else {
            return response()->json('The account specified does not belongs to you or not exists', 400);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBinanceAccountData(Request $request, BinanceAccount $binance_account_id): \Illuminate\Http\JsonResponse
    {
        if ($binance_account_id->user_id !== Auth::user()->id) {
            return response()->json('The account specified does not belongs to you or not exists', 400);
        }
        $binance_account_id->load('strategy');
        return response()->json(Helper::getBinanceAccountsDataAPI($binance_account_id), 200);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStrategies(Request $request): \Illuminate\Http\JsonResponse
    {
        return response()->json(Strategy::all(), 200);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrderStatues(Request $request): \Illuminate\Http\JsonResponse
    {
        return response()->json(OrderStatus::all(), 200);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBinanceAccounts(Request $request): \Illuminate\Http\JsonResponse
    {
        if (Auth::check()) {
            $binance_accounts = BinanceAccount::query()->where('user_id', Auth::user()->id)->with('strategy')->get();
            return response()->json($binance_accounts, 200);
        }
        abort(403, 'Forbidden');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFriends(Request $request): \Illuminate\Http\JsonResponse
    {
        if (Auth::check()) {
            $referral_id_owner = Auth::user()->referral_id_owner;
            $users = User::query()->where('referral_id', $referral_id_owner)->get();
            $friends = [];
            foreach ($users as $user) {
                $user_id = $user->id;
                $friends[] = [
                    'id' => $user_id,
                    'traded' => Order::query()
                        ->whereNotNull('related_order_id')
                        ->whereHas('binanceAccount', function ($query) use ($user_id) {
                            $query->where('user_id', $user_id);
                        })
                        ->exists(),
                    'created_at' => $user->created_at->format('d/m/Y H:i'),
                ];
            }
            return response()->json($friends, 200);
        }
        abort(403, 'Forbidden');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLastPrices(Request $request): \Illuminate\Http\JsonResponse
    {
        if (Auth::check()) {
            $last_prices = Price::query()->latest('id')->first();
            if (!$last_prices) {
                Artisan::call('trading:log-prices');
                sleep(1);
                $last_prices = Price::query()->latest('id')->first();
            }
            return response()->json($last_prices->original_prices, 200);
        }
        abort(403, 'Forbidden');
    }

}
