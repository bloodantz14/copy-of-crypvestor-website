<?php

namespace App\Http\Controllers;

use App\Helpers\BinanceHelper;
use App\Jobs\MarketSell;
use App\Models\BinanceAccount;
use App\Models\Order;
use App\Models\OrderGroup;
use App\Models\PreOrder;
use App\Models\Price;
use App\Models\Rule;
use App\Models\Strategy;
use App\Models\User;
use App\PackageOverrides\BinanceOverride;
use App\Trading\Helper;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rules\RequiredIf;
use Symfony\Component\Console\Input\Input;

class BinanceAdminController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function storePreOrders(Request $request): JsonResponse
    {
        $rules = [
            //'strategy' => 'required|integer|exists:strategies,id',
            'binance_accounts' => 'required|array',
            'binance_accounts.*' => 'required|integer|exists:binance_accounts,id',
            'symbol' => 'required|string',
            'rule_id' => 'required|integer|exists:rules,id',
            'value' => [new RequiredIf($request->rule_id != 3)],
            'stop_loss_percent' => 'nullable|numeric',
            'take_profit_percent' => 'nullable|numeric',
            'size_percent' => [
                'required_without:size_value',
                function ($attribute, $value, $fail) use ($request) {
                    if (!empty($value) && !empty($request->size_value)) {
                        $fail('Either Size percent or value should be is empty.');
                    }
                },
            ],
            'size_value' => [
                'required_without:size_percent',
                function ($attribute, $value, $fail) use ($request) {
                    if (!empty($value) && !empty($request->size_percent)) {
                        $fail('Either Size value or percent should be is empty.');
                    }
                },
            ],
        ];

        if( !empty($request->value) ){
            $rules['value'][] = 'numeric';
        }

        $data = $this->validate($request, $rules);

        $order_group = OrderGroup::query()->create([
            'symbol' => $data['symbol'],
            'size_amount' => $data['size_value'],
            'size_percent' => $data['size_percent'],
            'rule_id' => $data['rule_id'],
            'trigger_price' => $data['value'],
        ]);


        foreach ($data['binance_accounts'] as $account) {
            PreOrder::query()
                ->create([
                    'symbol' => $data['symbol'],
                    'binance_account_id' => $account,
                    'size_amount' => $data['size_value'],
                    'size_percent' => $data['size_percent'],
                    'rule_id' => $data['rule_id'],
                    'trigger_price' => $data['value'],
                    'stop_loss_percent' => $data['stop_loss_percent'],
                    'take_profit_percent' => $data['take_profit_percent'],
                    'order_group_id' => $order_group->id
                ]);
        }

        return response()->json('Pre orders created successfully', 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getPreOrders(Request $request): JsonResponse
    {
        $preorders = PreOrder::query()
            ->whereHas('binanceAccount')
            ->with([
                'binanceAccount.strategy',
                'binanceAccount.user',
                'rule'
            ])
            ->orderByDesc('id')
            ->get();

        return response()->json($preorders, 200);
    }

    /**
     * @param Request $request
     * @param \App\Models\PreOrder $pre_order_id
     * @return JsonResponse
     * @throws \Exception
     */
    public function deletePreOrders(Request $request, PreOrder $pre_order_id): JsonResponse
    {
        $pre_order_id->delete();
        return response()->json('Deleted Successfully', 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function deleteAllPreOrders(Request $request): JsonResponse
    {
        PreOrder::query()->delete();
        return response()->json('Deleted Successfully', 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getOpenOrders(Request $request): JsonResponse
    {
        $open_orders = Order::query()
            ->whereHas('binanceAccount')
            ->with([
                'binanceAccount.strategy',
                'binanceAccount.user',
                'preOrderTrashed',
            ])
            ->whereNull('related_order_id')
            ->where('side', 'BUY')
            ->orderByDesc('id')
            ->get();

        return response()->json($open_orders, 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getClosedOrders(Request $request): JsonResponse
    {
        $closed_orders = Order::query()
            ->with([
                'binanceAccount.strategy',
                'binanceAccount.user',
                'relatedOrder.binanceAccount.user',
                'relatedOrder.binanceAccount.strategy',
                'preOrderTrashed',
            ])
            ->whereHas('relatedOrder')
            ->whereHas('binanceAccount')
            ->where('side', 'BUY')
            ->orderByDesc('created_at')
            ->get();

        return response()->json($closed_orders, 200);
    }


    /**
     * @param Request $request
     * @param \App\Models\PreOrder $open_order_id
     * @return JsonResponse
     * @throws \Exception
     */
    public function deleteOpenOrders(Request $request, Order $open_order_id): JsonResponse
    {
        $open_order_id->delete();
        return response()->json('Deleted Successfully', 200);
    }


    public function getPnl(Request $request, $start_date, $binance_account_id = false)
    {
        if ($binance_account_id) {
            $binance_account_id = filter_var($binance_account_id, FILTER_SANITIZE_NUMBER_INT);
        }

        $start_date = Carbon::parse($start_date);

        $closed_orders = Order::query()
            ->with([
                'binanceAccount.strategy',
                'binanceAccount.user',
                'relatedOrder.binanceAccount.user',
                'relatedOrder.binanceAccount.strategy'
            ])
            ->whereHas('relatedOrder')
            ->whereHas('relatedOrder', function ($query) use ($start_date) {
                $query->whereDate('created_at', '>=', $start_date);
            })
            ->where('side', 'BUY');

        if ($binance_account_id) {
            $closed_orders = $closed_orders->where('binance_account_id', $binance_account_id);
        }

        $closed_orders = $closed_orders
            ->limit(500)
            ->orderBy('transact_time')
            ->get();

        $yaxis = [];
        $xaxis = [];
        $sum_pnl = 0;
        foreach ($closed_orders as $order) {
            if (
                is_null($order->relatedOrder->executed_qty)
                || is_null($order->relatedOrder->average_price)
                || is_null($order->executed_qty)
                || is_null($order->average_price)
            ) {
                continue;
            }
            $pnl = ($order->relatedOrder->executed_qty * $order->relatedOrder->average_price)
                - ($order->executed_qty * $order->average_price);
            $roe = ($order->relatedOrder->average_price - $order->average_price) / $order->average_price  * 100;
            $sum_pnl += $pnl;
            $yaxis['data'][] = number_format((float)$sum_pnl, 2, '.', '');
            $yaxis['extra'][] = [
                'sum' => number_format((float)$sum_pnl, 2),
                'pnl' => number_format((float)$pnl, 2),
                'roe' => number_format((float)$roe, 2)
            ];
            $xaxis[] = $order->relatedOrder->created_at->format('d/m H:i');
        }

        $data = [
            'series' => $yaxis,
            'datasets' => $xaxis,
        ];

        return response()->json($data, 200);
    }

    /**
     * @param Request $request
     * @param \App\Models\Order $open_order_id
     * @return JsonResponse
     * @throws \Exception
     */
    public function updateOpenOrders(Request $request, Order $open_order_id): JsonResponse
    {
        $this->validate($request, [
            'stop_percent' => 'nullable|numeric',
            'profit_percent' => 'nullable|numeric',
        ]);
        $open_order_id->update($request->all());
        return response()->json('Updated Successfully', 200);
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateOpenOrdersBulk(Request $request): JsonResponse
    {
        $data = $this->validate($request, [
            'order_ids' => 'array',
            'order_ids.*' => 'required|integer|exists:orders,id',
            'stop_percent' => 'nullable|numeric',
            'profit_percent' => 'nullable|numeric',
        ]);

        $stop_loss_percent = trim($data['stop_percent']);
        if (is_numeric($stop_loss_percent) ) {
            Order::query()->whereIn('id', $data['order_ids'])
                ->update([
                    'stop_percent' => $stop_loss_percent,
                ]);
        }

        $take_profit_percent = trim($data['profit_percent']);
        if (is_numeric($take_profit_percent)) {
            Order::query()->whereIn('id', $data['order_ids'])
                ->update([
                    'profit_percent' => $take_profit_percent,
                ]);
        }
        return response()->json('Updated Successfully', 200);
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function sellAllOpenOrders(Request $request): JsonResponse
    {
        $open_orders = Order::query()
            ->with(['binanceAccount.user'])
            ->whereNull('related_order_id')
            ->where('side', 'BUY')
            ->orderByDesc('id')
            ->get();
        foreach ($open_orders as $open_order) {
            MarketSell::dispatch($open_order);
        }
        return response()->json('Jobs created...', 200);
    }

    /**
     * @param Request $request
     * @param \App\Models\Order $open_order_id
     * @return JsonResponse
     * @throws \Exception
     */
    public function sellOrderNow(Request $request, Order $open_order_id): JsonResponse
    {
        MarketSell::dispatch($open_order_id);
        return response()->json('A Market Sell task been created', 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getBinanceAccountsData(Request $request): JsonResponse
    {
        $accounts = BinanceAccount::query()
            ->with('strategy')
            ->get();

        return response()->json(Helper::getBinanceAccountsDataAPI($accounts), 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getBinanceAccounts(Request $request): JsonResponse
    {
        if (Auth::user()->isAdmin()) {
            $binance_accounts = BinanceAccount::query()
                ->with(['strategy', 'user'])
                ->get()
                ->groupBy('user_id');
            return response()->json($binance_accounts, 200);
        }
        abort(403, 'Forbidden');
    }

    /**
     * This method will filter those accounts that are inactive for trading.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getClients(Request $request): JsonResponse
    {
        if (Auth::user()->isAdmin()) {
            $clients = User::query()
                ->withCount(['binanceAccountsActive'])
                ->with([
                    'binanceAccountsActive.strategy',
                    'binanceAccountsActive.lastBalance'
                ])
                ->whereHas('binanceAccountsActive')
                ->orderByDesc('binance_accounts_active_count')
                ->get();
            return response()->json($clients, 200);
        }
        abort(403, 'Forbidden');
    }

    /**
     * This method will filter those accounts that are inactive for trading.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getClientsAll(Request $request): JsonResponse
    {
        if (Auth::user()->isAdmin()) {
            $clients = User::query()
                ->withCount(['binanceAccounts'])
                ->with(['binanceAccounts.strategy'])
                ->orderByDesc('binance_accounts_count')
                ->get();
            return response()->json($clients, 200);
        }
        abort(403, 'Forbidden');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getOrderRules(Request $request): JsonResponse
    {
        if (Auth::user()->isAdmin()) {
            $order_rules = Rule::all();
            return response()->json($order_rules, 200);
        }
        abort(403, 'Forbidden');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getOrderGroups(Request $request): JsonResponse
    {
        if (Auth::user()->isAdmin()) {
            $order_groups = OrderGroup::query()
                ->whereHas('orders', function ($query) {
                    $query->whereDoesntHave('relatedOrder');
                })
                ->get();
            return response()->json($order_groups, 200);
        }
        abort(403, 'Forbidden');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getClientBinanceAccountAPI(Request $request, BinanceAccount $binance_account_id): JsonResponse
    {
        if (Auth::user()->isAdmin()) {
            $binance_account_id->load('strategy');
            return response()->json(Helper::getBinanceAccountsDataAPI($binance_account_id), 200);
        }
        abort(403, 'Forbidden');
    }
}
