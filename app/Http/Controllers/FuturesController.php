<?php

namespace App\Http\Controllers;

use App\Helpers\BinanceHelper;
use App\Models\BinanceAccount;
use App\Models\FuturesIncome;
use App\Models\Price;
use App\Models\Strategy;
use App\PackageOverrides\BinanceOverride;
use App\Trading\Helper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;

class FuturesController extends Controller
{

    public function getFuturesIncome(Request $request)
    {
        $incomes = FuturesIncome::query()
            ->where('time', '>=', Carbon::parse('2021-09-11 12:00:00'))
            ->whereNotNull('tran_id')
            ->where('income_type', '!=', 'TRANSFER')
            ->get();

        $xaxis = [];
        $yaxis = [];
        $sum_pnl = 0;
        foreach ($incomes as $income) {
            $date = $income->time->format('d/m H:i');
            $type = $income->income_type;
            $sum_pnl += $income->income;

            $xaxis[] = $date;
            $yaxis['data'][] = number_format((float)$sum_pnl, 2, '.', '');
            $yaxis['extra'][] = [
                'sum' => number_format((float)$sum_pnl, 2, '.', ''),
                'pnl' => number_format((float)$income->income, 2, '.', ''),
                'type' => $type,
                'symbol' => $income->symbol
            ];
        }

        $data = [
            'series' => $yaxis,
            'datasets' => $xaxis,
        ];

        return response()->json($data, 200);
    }

}
