<?php

namespace App\Http\Controllers;

use App\Helpers\BinanceHelper;
use App\Models\BinanceAccount;
use App\Models\Strategy;
use App\Notifications\NewContact;
use Binance\API;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class GenericController extends Controller
{

    public function sendContactMessage(Request $request)
    {
        $contact = $this->validate($request, [
            "name" => "required|string",
            "email" => "required|email",
            "country" => "required|string",
            "message" => "required|string",
        ]);

        // To simulate object for send Email.
        $contactObj = json_decode(json_encode($contact), FALSE);

        Notification::route('mail', 'hola@crypvestor.com')->notify(new NewContact($contactObj));

        // Set timestamp manually.
        $contact['created_at'] = \Carbon\Carbon::now();
        $contact['updated_at'] = \Carbon\Carbon::now();

        DB::transaction(function () use ($contact) {
            DB::table('contact_us_form')->insert($contact);
        });

        return response()->json('Your message has been sent successfully!', 200);
    }

}
