<?php

namespace App\Http\Controllers;

use App\Helpers\BinanceHelper;
use App\Models\BinanceAccount;
use App\Models\Price;
use App\Models\Strategy;
use App\PackageOverrides\BinanceOverride;
use App\Trading\Helper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;

class StatusController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLogLastPrices(): \Illuminate\Http\JsonResponse
    {
        $last_prices = Price::query()->latest('id')->first();
        $prices = [
            'created_at' => $last_prices->created_at,
            'diff_humans' => $last_prices->created_at->diffForHumans(),
            'prices' => array_slice($last_prices->prices->toArray(), 0, 3),
        ];
        $prices['prices']['MORE'] = '...';
        return response()->json($prices, 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSupervisorStatus(): \Illuminate\Http\JsonResponse
    {
        $path = base_path('bash/cmd-supervisor-status-all.sh');
        $command = ". {$path}";
        $output = shell_exec("$command 2>&1; echo $?");
        return response()->json($output, 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function restartProcesses(): \Illuminate\Http\JsonResponse
    {
        $path = base_path('bash/cmd-restart-all-process.sh');
        $command = ". {$path}";
        $output = shell_exec("$command 2>&1; echo $?");
        return response()->json($output, 200);
    }

}
