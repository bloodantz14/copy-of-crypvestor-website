<?php

namespace App\Helpers;

use App\PackageOverrides\BinanceOverride;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class BinanceHelper
{
    public static function getUsdtTotal($balances, $prices)
    {
        $usdt_total = 0;
        foreach ($balances as $coin => $balance) {
            if ($coin === 'USDT') {
                $coin_total = $balance['available'] + $balance['onOrder'];
                $usdt_total = $usdt_total + $coin_total;
            } else if ($balance['available'] > 0 || $balance['onOrder'] > 0) {
                $symbol = $coin.'USDT';
                $coin_total = $balance['available'] + $balance['onOrder'];
                $current_price = $prices[$symbol] ?? 0;
                $usdt = $coin_total * $current_price;
                $usdt_total = $usdt_total + $usdt;
            }
        }
        return $usdt_total;
    }

    /**
     * Get Available USDT Balance
     * @param $balances
     * @return float|int|mixed
     */
    public static function getUsdtAvailable($balances)
    {
        foreach ($balances as $coin => $balance) {
            if ($coin === 'USDT') {
                return $balance['available'];
            }
        }
        return 0;
    }


    public static function getBalanceTotals($balances)
    {
        $totals = [];
        foreach ($balances as $coin => $balance) {
            $totals[$coin] = $balance['available'] + $balance['onOrder'];
        }
        return $totals;
    }


    public static function getPriceInThePast($symbol, Carbon $time)
    {
        $timestamp = $time->startOfMinute()->timestamp * 1000;
        if (config('binance.use_testnet')) {
            $candle = json_decode(file_get_contents("https://testnet.binance.vision/api/v3/klines?symbol={$symbol}&interval=1m&limit=1&startTime={$timestamp}"), true);
        } else {
            $candle = json_decode(file_get_contents("https://api.binance.com/api/v3/klines?symbol={$symbol}&interval=1m&limit=1&startTime={$timestamp}"), true);
        }
        return $candle;
        $close_price = false;
        if (!empty($candle)) {
            $close_price = $candle[0][4];
        }
        return $close_price;
    }

    public static function getPrices()
    {
        if (config('binance.use_testnet')) {
            $prices = file_get_contents('https://testnet.binance.vision/api/v3/ticker/price');
        } else {
            $prices = file_get_contents('https://api.binance.com/api/v3/ticker/price');
        }

        return collect(json_decode($prices, true))->filter(function ($price) {
            return strstr($price['symbol'], 'USDT');
        })->values()->toJson();
    }

    /**
     * @throws \Exception
     */
    public static function getExchangeInfo(BinanceOverride $api)
    {
        $exchange_info = Cache::get('exchangeInfo');
        if (!$exchange_info) {
            $exchange_info = $api->exchangeInfo();
            Cache::put('exchangeInfo', $exchange_info, now()->addMinutes(15));
        }
        return $exchange_info;
    }
}
