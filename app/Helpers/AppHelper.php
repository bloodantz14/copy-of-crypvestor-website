<?php


namespace App\Helpers;

use App\IFTTT\IFTTT;

class AppHelper
{
    /**
     * @param $title
     * @param $message
     * @param null $link
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public static function notifyIFTT($title, $message, $link = null, $only_key_keys = [])
    {
        $ifttt = app()->make(IFTTT::class);
        $payload = [
            'value1' => $title,
            'value2' => $message,
        ];
        if ($link) {
            $payload['value3'] = $link;
        }
        $ifttt->publishEvent(config('ifttt.event_name'), $payload, $only_key_keys);
    }

    /**
     * @param $length
     * @return string
     */
    public static function secureRandomString($length) {
        $random_string = '';
        for($i = 0; $i < $length; $i++) {
            try {
                $number = random_int(0, 36);
            } catch (\Exception $e) {
            }
            $character = base_convert($number, 10, 36);
            $random_string .= $character;
        }

        return strtoupper($random_string);
    }
}
