<?php

namespace App\Jobs;

use App\Helpers\AppHelper;
use App\Helpers\BinanceHelper;
use App\Models\BinanceAccount;
use App\Models\Order;
use App\Models\PreOrder;
use App\PackageOverrides\BinanceOverride;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class MarketSell implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Symbol
     * @var string
     */
    public $symbol = null;

    /**
     * @var BinanceOverride
     */
    public BinanceOverride $api;

    /**
     * Balances
     * @var array
     */
    public array $balances;

    /**
     * Exchange Info
     * @var array
     */
    public array $exchange_info;

    /**
     * @var array
     */
    private array $filters = [];

    /**
     * @var float
     */
    private float $order_size = 0;

    /**
     * @var string
     */
    private string $order_size_type = '';

    /**
     * Always leave some money in the account to prevent issues with the filters and Min qty
     * @var float
     */
    private float $amount_no_trade = 5; // usdt

    /**
     * Minimum amount required to trade
     * @var float
     */
    private float $minimum_amount_to_trade = 10; // usdt

    /**
     * You can pass an object that implements
     * @var bool|object implements AfterMarketSell
     */
    private $callback_after_sell;

    /**
     * @var mixed
     */
    private ?int $binance_account_id = null;


    /**
     * @var \App\Models\Order
     */
    private \App\Models\Order $open_order;

    /**
     * Create a new job instance.
     *
     * @return void
     * @throws \Exception
     */
    public function __construct($open_order, $callback_after_sell = false)
    {
        $this->open_order = $open_order;

        if ($this->open_order->binanceAccount) {
            $this->api = new BinanceOverride(
                $this->open_order->binanceAccount->getRawOriginal('public_key'),
                $this->open_order->binanceAccount->getRawOriginal('secret_key')
            );
            $this->binance_account_id = $this->open_order->binance_account_id;
        } else {
            $this->api = new BinanceOverride(
                config('binance.default_api_key'),
                config('binance.default_api_secret')
            );
        }

        $this->symbol = $this->open_order->symbol;

        if ($callback_after_sell) {
            $this->callback_after_sell = $callback_after_sell;
        }

        $this->api->useServerTime();
        $this->setBalances();
        $this->setExchangeInfo();
        $this->setFilters();
    }

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function handle()
    {
        $this->sell();
        //AppHelper::notifyIFTT('Crypvestor', 'Sold successfully');
    }

    public function sell()
    {
        try {
            $coin = str_replace('USDT', '', $this->open_order->symbol);
            $previous_executed_qty = $this->open_order->executed_qty;
            $coin_balance = $this->balances[$coin]['available'] ?? 0;
            //$coin_balance_original = $coin_balance;
            $amount_to_trade = $coin_balance < $previous_executed_qty ? $coin_balance : $previous_executed_qty;
            $amount_to_trade_original = $amount_to_trade;

            if ((float)$this->filters['LOT_SIZE']['stepSize'] > 0) {
                $amount_to_trade = $this->api->roundStep($amount_to_trade, (float)$this->filters['LOT_SIZE']['stepSize']);
            }

            if ($amount_to_trade_original < $amount_to_trade) {
                $decimal_count = strlen(substr(strrchr($amount_to_trade, "."), 1));
                $sum = '0.';
                foreach (range(0, $decimal_count-2) as $item) {
                    $sum .= '0';
                }
                $sum .= '1';
                $amount_to_trade -= $sum;
            }

            $order = $this->api->marketSell($this->open_order->symbol, $amount_to_trade);
            if ($order['status'] !== 'FILLED' && $order['status'] !== 'CANCELED') {
                //This part is untested because the orders are always filled straight away
                Log::info('Sell order not filled, status' . $order['status'] . '. We will check every second until its filled or cancelled.');
                while ($order['status'] !== 'FILLED' && $order['status'] !== 'CANCELED') {
                    sleep(1);
                    $order = $this->api->orderStatus($this->open_order->symbol, $order['orderId']);
                    Log::info('Sell order status now: ' . $order['status']);
                }
            }

            $this->registerOrder($order);

            if ($this->callback_after_sell) {
                $this->callback_after_sell->afterSell($order);
            }

        } catch (\Exception $e) {
            $exception_message = $e->getMessage();
            Log::error("Seller Error {$exception_message}");
            AppHelper::notifyIFTT("Seller Error", $exception_message);

            // if a pre-order was given
            if ($this->open_order) {
                $this->open_order->update([
                    'internal_status' => 'error',
                    'internal_details' => $exception_message
                ]);
            }
        }
    }

    public function registerOrder($order)
    {
        $order = Order::query()->create([
            'fills' => json_encode($order['fills']),
            'symbol' => $order['symbol'],
            'order_id' => $order['orderId'],
            'order_list_id' => $order['orderListId'],
            'client_order_id' => $order['clientOrderId'],
            'transact_time' => $order['transactTime'],
            'price' => $order['price'],
            'orig_qty' => $order['origQty'],
            'executed_qty' => $order['executedQty'],
            'cumulative_quote_qty' => $order['cummulativeQuoteQty'],
            'status' => $order['status'],
            'time_in_force' => $order['timeInForce'],
            'type' => $order['type'],
            'side' => $order['side'],
            'related_order_id' => $this->open_order->id,
            'binance_account_id' => $this->binance_account_id,
        ]);
        //print_r($this->pre_order->toArray());
        // if a pre-order was given
        $this->open_order->update([
            'internal_status' => 'sold',
            'related_order_id' => $order->id
        ]);

        $binance_account = BinanceAccount::query()->with('user')->find($this->binance_account_id);
        if ($binance_account) {
            AppHelper::notifyIFTT("Selling {$order['symbol']}", "Account: {$binance_account->name} | Client: {$binance_account->user->name}");
        } else {
            AppHelper::notifyIFTT("Selling {$order['symbol']}", "No account selected ????");
        }
    }

    public function setBalances()
    {
        $this->balances = $this->api->balances();
    }

    /**
     * @throws \Exception
     */
    public function setExchangeInfo()
    {
        $this->exchange_info = BinanceHelper::getExchangeInfo($this->api);
    }

    /**
     * @throws \Exception
     */
    public function getAmountToTrade($available_usdt)
    {
        return $available_usdt - $this->amount_no_trade;;
    }

    /**
     *
     * @throws \Exception
     */
    public function setFilters()
    {
        $this->filters = [];
        foreach ($this->exchange_info['symbols'] as $filters) {
            if ($filters['symbol'] ===  $this->symbol) {
                $this->filters = array_column($filters['filters'], null, 'filterType');
                if ($this->filters === null) {
                    throw new \Exception('Could not find the trade filters');
                }
                break;
            }
        }
    }
}
