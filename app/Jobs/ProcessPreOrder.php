<?php

namespace App\Jobs;

use App\Helpers\AppHelper;
use App\Models\Price;
use App\Models\Rule;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use function Symfony\Component\Translation\t;

class ProcessPreOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $pre_order;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($pre_order)
    {
        /**
         * :::::::NOT IN USE:::::::
         */
        die('use the command TradingProcessPreOrders::class instead.');

        $this->pre_order = $pre_order;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle($attempt = 1)
    {
        $prices = Price::query()->latest('id')->first();
        if (now()->subMinute() > $prices->created_at) {
            if ($attempt > 2 ) {
                $this->logError(__LINE__);
                return;
            }
            Artisan::call('trading:log-prices');
            sleep(2);
            $this->handle($attempt++);
        }

        $symbol = $this->pre_order->symbol;
        $price = $prices->prices[$symbol] ?? false;
        if (!$price) {
            $this->logError(__LINE__);
            return;
        }

        switch ($this->pre_order->rule_id) {
            case Rule::UNDER:
                if (empty($this->pre_order->trigger_price)) {
                    throw new \Exception('Trigger price should be not empty');
                }
                // under with price
                if ($this->pre_order->trigger_price > $price) {
                    $this->pre_order->update(['status' => 'processing']);
                    MarketBuy::dispatch($this->pre_order)->onQueue('high');
                }
                break;

            case Rule::OVER:
                if (empty($this->pre_order->trigger_price)) {
                    $this->logError(__LINE__);
                    throw new \Exception('Trigger price should be not empty');
                }
                // under with price
                if ($this->pre_order->trigger_price < $price) {
                    $this->pre_order->update(['status' => 'processing']);
                    MarketBuy::dispatch($this->pre_order)->onQueue('high');
                }
                break;
        }
    }

    /**
     * @param $line
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function logError($line)
    {
        $message = 'Cannot process a Pre Order in Job (LINE: '.$line.'): Preorder ID: ' . $this->pre_order->id;
        Log::emergency($message);

        $this->pre_order->update([
            'status' => 'error',
            'details' => $message
        ]);

        if (!Cache::has('notification-job-pre-order')) {
            AppHelper::notifyIFTT('Crypvestor', $message);
            Cache::add('notification-job-pre-order', true, 60 * 30); // half an hour
        }
    }
}
