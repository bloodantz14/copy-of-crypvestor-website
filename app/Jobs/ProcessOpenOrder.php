<?php

namespace App\Jobs;

use App\Helpers\AppHelper;
use App\Models\Price;
use App\Models\Rule;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class ProcessOpenOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $open_order;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($open_order)
    {
        /**
         * :::::::NOT IN USE:::::::
         */
        die('use the command TradingProcessOpenOrders::class instead.');


        $this->open_order = $open_order;
    }

    /**
     * @param int $attempt
     */
    public function handle(int $attempt = 1)
    {
        $prices = Price::query()->latest('id')->first();
        if (now()->subMinute() > $prices->created_at) {
            if ($attempt > 2 ) {
                $this->logError(__LINE__);
                return;
            }
            Artisan::call('trading:log-prices');
            sleep(2);
            $this->handle($attempt++);
        }

        $symbol = $this->open_order->symbol;
        $price = $prices->prices[$symbol] ?? false;
        if (!$price) {
            $this->logError(__LINE__);
            return;
        }

        if ($this->open_order->stop_loss_price && $this->open_order->stop_loss_price >= $price) {
            MarketSell::dispatch($this->open_order)->onQueue('high');
        }

        elseif ($this->open_order->take_profit_price && $this->open_order->take_profit_price <= $price) {
            MarketSell::dispatch($this->open_order)->onQueue('high');
        }
    }

    /**
     * @param $line
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function logError($line)
    {
        $message = 'Cannot process a Open Order in Job (LINE: '.$line.'): Preorder ID: ' . $this->open_order->id;
        Log::emergency($message);

        if (!Cache::has('notification-job-pre-order')) {
            AppHelper::notifyIFTT('Crypvestor', $message);
            Cache::add('notification-job-pre-order', true, 60 * 30); // half an hour
        }
    }
}
