<?php

namespace App\Jobs;

use App\Helpers\AppHelper;
use App\Helpers\BinanceHelper;
use App\Models\BinanceAccount;
use App\Models\Order;
use App\Models\PreOrder;
use App\PackageOverrides\BinanceOverride;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class MarketBuy implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Symbol
     * @var string
     */
    public $symbol = null;

    /**
     * @var PreOrder
     */
    private PreOrder $pre_order;

    /**
     * @var BinanceOverride
     */
    public BinanceOverride $api;

    /**
     * Balances
     * @var array
     */
    public array $balances;

    /**
     * Exchange Info
     * @var array
     */
    public array $exchange_info;

    /**
     * @var array
     */
    private array $filters = [];

    /**
     * @var float
     */
    private float $order_size = 0;

    /**
     * @var string
     */
    private string $order_size_type = '';

    /**
     * Always leave some money in the account to prevent issues with the filters and Min qty
     * @var float
     */
    private float $amount_no_trade = 20; // usdt

    /**
     * Minimum amount required to trade
     * @var float
     */
    private float $minimum_amount_to_trade = 40; // usdt

    /**
     * You can pass an object that implements
     * @var bool|object implements AfterMarketBuy
     */
    private $callback_after_buy;

    /**
     * @var mixed
     */
    private ?int $binance_account_id = null;

    /**
     * Create a new job instance.
     *
     * @param $object PreOrder|BinanceAccount or add false if custom buy is what you want
     * @param $data array | [symbol, size_type, size_amount], not required if a pre-order is given
     * @param $callback_after_buy bool|object -> callback after bought
     * @throws \Exception
     */
    public function __construct($object, array $data = [], $callback_after_buy = false)
    {
        // wants to buy wit from a pre-order
        if ($object instanceof PreOrder) {
            $this->pre_order = $object;
            $this->api = new BinanceOverride(
                $this->pre_order->binanceAccount->getRawOriginal('public_key'),
                $this->pre_order->binanceAccount->getRawOriginal('secret_key')
            );
            $this->symbol = $this->pre_order->symbol;
            $this->binance_account_id = $this->pre_order->binance_account_id;

            if (!empty($this->pre_order->size_percent)) {
                $this->order_size = $this->pre_order->size_percent;
                $this->order_size_type = 'percent';
            } elseif (!empty($this->pre_order->size_amount)) {
                $this->order_size = $this->pre_order->size_amount;
                $this->order_size_type = 'amount';
            } else {
                throw new \Exception('Either Size Percent or amount is required');
            }
        }

        else {

            // wants to buy from an client account
            if($object instanceof BinanceAccount) {
                $this->api = new BinanceOverride(
                    $object->getRawOriginal('public_key'),
                    $object->getRawOriginal('secret_key'),
                );
                $this->binance_account_id = $object->id;
            }

            // wants to buy from default keys defined in config area.
            else  {
                $this->api = new BinanceOverride(
                    config('binance.default_api_key'),
                    config('binance.default_api_secret')
                );
            }

            if (empty($data['symbol'])) {
                throw new \Exception('No symbol supplied');
            }
            $this->symbol = $data['symbol'];

            if (!empty($data['size_percent'])) {
                $this->order_size = $data['size_percent'];
                $this->order_size_type = 'percent';
            } elseif (!empty($data['size_amount'])) {
                $this->order_size = $data['size_amount'];
                $this->order_size_type = 'amount';
            } else {
                throw new \Exception('Either Size Percent or amount is required');
            }
        }

        if ($callback_after_buy) {
            $this->callback_after_buy = $callback_after_buy;
        }

        $this->api->useServerTime();
        $this->setBalances();
        $this->setExchangeInfo();
        $this->setFilters();
    }

    public function handle()
    {
        $this->buy();
    }


    public function setBalances()
    {
        $this->balances = $this->api->balances();
    }

    /**
     * @throws \Exception
     */
    public function setExchangeInfo()
    {
        $this->exchange_info = BinanceHelper::getExchangeInfo($this->api);
    }

    /**
     * @throws \Exception
     */
    public function getAmountToTrade($available_usdt)
    {
        $amount_to_trade = $available_usdt - $this->amount_no_trade;

        if ($amount_to_trade < $this->minimum_amount_to_trade) {
            throw new \Exception('I don\'t think you have enough balance to continue buying');
        }

        if ($this->order_size_type == 'percent') {
            $amount_to_trade = $amount_to_trade * ($this->order_size / 100);
        }
        if ($this->order_size_type == 'amount') {
            $amount_to_trade = $this->order_size > $available_usdt ? $available_usdt : $this->order_size;
        }
        return $amount_to_trade;
    }

    /**
     *
     * @throws \Exception
     */
    public function setFilters()
    {
        $this->filters = [];
        foreach ($this->exchange_info['symbols'] as $filters) {
            if ($filters['symbol'] ===  $this->symbol) {
                $this->filters = array_column($filters['filters'], null, 'filterType');
                if ($this->filters === null) {
                    throw new \Exception('Could not find the trade filters');
                }
                break;
            }
        }
    }

    public function registerOrder($order)
    {
        $order = Order::query()->create([
            'fills' => json_encode($order['fills']),
            'symbol' => $order['symbol'],
            'order_id' => $order['orderId'],
            'order_list_id' => $order['orderListId'],
            'client_order_id' => $order['clientOrderId'],
            'transact_time' => $order['transactTime'],
            'price' => $order['price'],
            'orig_qty' => $order['origQty'],
            'executed_qty' => $order['executedQty'],
            'cumulative_quote_qty' => $order['cummulativeQuoteQty'],
            'status' => $order['status'],
            'time_in_force' => $order['timeInForce'],
            'type' => $order['type'],
            'side' => $order['side'],
            'binance_account_id' => $this->binance_account_id,
            'profit_percent' => $this->pre_order->take_profit_percent ?? null,
            'stop_percent' => $this->pre_order->stop_loss_percent ?? null,
            'pre_order_id' => $this->pre_order->id ?? null,
            'order_group_id' => $this->pre_order->order_group_id ?? null
        ]);
        //print_r($this->pre_order->toArray());
        // if a pre-order was given
        if ($this->pre_order) {
            $this->pre_order->update(['status' => 'processed']);
            $this->pre_order->delete();
        }

        $binance_account = BinanceAccount::query()->with('user')->find($this->binance_account_id);
        if ($binance_account) {
            AppHelper::notifyIFTT("Buying {$order['symbol']}", "Account: {$binance_account->name} | Client: {$binance_account->user->name}");
        } else {
            AppHelper::notifyIFTT("Buying {$order['symbol']}", "No account selected ????");
        }
    }

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Exception
     */
    public function buy() {
        $available_usdt = $this->balances['USDT']['available'];

        try {
            // returns the safe stable amount to play
            $amount_to_trade = $this->getAmountToTrade($available_usdt);
            $price = (float) $this->api->price($this->symbol);

            $qty = $amount_to_trade / $price;
            if ((float) $this->filters['LOT_SIZE']['stepSize'] > 0) {
                $qty = $this->api->roundStep($qty, (float) $this->filters['LOT_SIZE']['stepSize']);
            }

            $order = $this->api->marketBuy($this->symbol, $qty);
            if ($order['status'] !== 'FILLED' && $order['status'] !== 'CANCELED') {
                //This part is untested because the orders are always filled straight away
                Log::info('Buy order not filled, status' . $order['status'] . '. We will check every second until its filled or cancelled.');
                while ($order['status'] !== 'FILLED' && $order['status'] !== 'CANCELED') {
                    sleep(1);
                    $order = $this->api->orderStatus($this->symbol, $order['orderId']);
                    Log::info('Buy order status now: ' . $order['status']);
                }
            }

            $this->registerOrder($order);

            if ($this->callback_after_buy) {
                $this->callback_after_buy->afterBuy($order);
            }

        } catch (\Exception $e) {
            $exception_message = $e->getMessage();
            Log::error("Buyer Error {$exception_message}");
            AppHelper::notifyIFTT("Buyer Error", $exception_message);

            // if a pre-order was given
            if ($this->pre_order) {
                $this->pre_order->update([
                    'status' => 'error',
                    'details' => $exception_message
                ]);
            }
        }

    }
}
