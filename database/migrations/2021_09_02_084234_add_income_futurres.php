<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIncomeFuturres extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('futures_incomes', function (Blueprint $table) {
            $table->id();
            $table->string('symbol')->nullable();
            $table->string('income_type')->nullable();
            $table->float('income')->nullable();
            $table->string('asset')->nullable();
            $table->timestamp('time')->nullable();
            $table->string('info')->nullable();
            $table->string('tran_id')->nullable();
            $table->string('trade_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('futures_incomes');
    }
}
