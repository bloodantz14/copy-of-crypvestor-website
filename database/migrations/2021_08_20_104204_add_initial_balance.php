<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInitialBalance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('binance_accounts', function (Blueprint $table) {
            $table->string('initial_balance_total')->nullable()->after('note');
            $table->text('initial_balances_array')->nullable()->after('initial_balance_total');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('binance_accounts', function (Blueprint $table) {
            $table->dropColumn('initial_balance_total');
            $table->dropColumn('initial_balances_array');
        });
    }
}
