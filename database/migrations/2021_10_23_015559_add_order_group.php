<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOrderGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_groups', function (Blueprint $table) {
            $table->id();
            $table->string('symbol');
            $table->float('size_amount')->nullable(); // order size
            $table->float('size_percent')->nullable();
            $table->string('rule_id')->nullable(); // rule, under or over, etc
            $table->float('trigger_price')->nullable(); // when the price is
            $table->timestamps();
        });

        Schema::table('pre_orders', function (Blueprint $table) {
            $table->unsignedBigInteger('order_group_id')->after('id')->nullable();
            $table->foreign('order_group_id')
                ->references('id')
                ->on('order_groups');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedBigInteger('order_group_id')->after('id')->nullable();
            $table->foreign('order_group_id')
                ->references('id')
                ->on('order_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pre_orders', function (Blueprint $table) {
            $table->dropForeign(['order_group_id']);
            $table->dropColumn('order_group_id');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['order_group_id']);
            $table->dropColumn('order_group_id');
        });
        Schema::dropIfExists('order_groups');
    }
}
