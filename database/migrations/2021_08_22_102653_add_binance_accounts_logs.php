<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBinanceAccountsLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('binance_account_balances', function (Blueprint $table) {
            $table->id();
            $table->float('total')->nullable();
            $table->longText('raw')->nullable();
            $table->unsignedBigInteger('binance_account_id');
            $table->timestamps();

            $table->foreign('binance_account_id')
                ->references('id')
                ->on('binance_accounts')
                ->onDelete(null);
        });
        Schema::create('binance_account_transactions', function (Blueprint $table) {
            $table->id();
            $table->string('tx_id')->nullable();
            $table->string('coin')->nullable();
            $table->float('amount')->nullable();
            $table->float('usdt_amount')->nullable();
            $table->string('address')->nullable();
            $table->timestamp('tx_time')->nullable();
            $table->string('status')->nullable();
            $table->string('type')->nullable();
            $table->unsignedBigInteger('binance_account_id');
            $table->timestamps();

            $table->foreign('binance_account_id')
                ->references('id')
                ->on('binance_accounts')
                ->onDelete(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('binance_account_balances');
        Schema::dropIfExists('binance_account_transactions');
    }
}
