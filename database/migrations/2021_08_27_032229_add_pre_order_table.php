<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPreOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_orders', function (Blueprint $table) {
            $table->id();
            $table->string('symbol');
            $table->unsignedBigInteger('binance_account_id');
            $table->float('size_amount')->nullable(); // order size
            $table->float('size_percent')->nullable();
            $table->string('rule_id')->nullable(); // rule, under or over, etc
            $table->float('trigger_price')->nullable(); // when the price is
            $table->string('status')->nullable();
            $table->text('details')->nullable();
            $table->float('stop_loss_percent')->nullable();
            $table->float('take_profit_percent')->nullable();
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('binance_account_id')
                ->references('id')
                ->on('binance_accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_orders');
    }
}
