<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBillingStatusId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_statuses', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug');
            $table->timestamps();
        });

        \App\Models\OrderStatus::query()->insert([
            ['name' => 'To be Paid',         'slug' => 'to_be_paid'],
            ['name' => 'Pending Payment',    'slug' => 'pending_payment'],
            ['name' => 'Paid',               'slug' => 'paid'],
            ['name' => 'Overdue',            'slug' => 'overdue'],
        ]);

        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedBigInteger('order_status_id')->nullable();
            $table->foreign('order_status_id')->references('id')->on('order_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['order_status_id']);
            $table->dropColumn('order_status_id');
        });
        Schema::dropIfExists('order_statuses');
    }
}
