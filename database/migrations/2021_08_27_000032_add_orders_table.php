<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->id();
            $table->json('prices')->nullable();
            $table->timestamps();
        });
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('binance_account_id')->nullable();
            $table->string('symbol');
            $table->string('order_id');
            $table->string('transact_time')->nullable();
            $table->string('price')->nullable();
            $table->string('orig_qty')->nullable();
            $table->string('executed_qty')->nullable();
            $table->string('cumulative_quote_qty')->nullable();
            $table->string('status')->nullable();
            $table->string('time_in_force')->nullable();
            $table->float('stop_price')->nullable();
            $table->float('stop_percent')->nullable();
            $table->float('profit_amount')->nullable();
            $table->float('profit_percent')->nullable();
            $table->string('type')->nullable();
            $table->string('side')->nullable();
            $table->json('fills')->nullable();
            $table->string('internal_status')->nullable();
            $table->text('internal_details')->nullable();
            $table->timestamps();


            $table->foreign('binance_account_id')
                ->references('id')
                ->on('binance_accounts');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedBigInteger('related_order_id')->after('fills')->nullable();
            $table->foreign('related_order_id')
                ->references('id')
                ->on('orders');
        });

        Schema::create('order_logs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('order_id');
            $table->foreign('order_id')
                ->references('id')
                ->on('orders');
            $table->float('price')->nullable();
            $table->float('profit')->nullable();
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prices');
        Schema::dropIfExists('order_logs');
        Schema::dropIfExists('orders');
    }
}
