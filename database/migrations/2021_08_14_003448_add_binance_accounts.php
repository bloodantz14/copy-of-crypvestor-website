<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Strategy;

class AddBinanceAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('strategies', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description')->nullable();
            $table->timestamps();
        });
        Schema::create('binance_accounts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('name');
            $table->string('public_key');
            $table->string('secret_key');
            $table->boolean('active')->default(1);
            $table->unsignedBigInteger('strategy_id');
            $table->string('note')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('strategy_id')
                ->references('id')
                ->on('strategies')
                ->onDelete('cascade');
        });

        $faker = Faker\Factory::create();
        Strategy::query()->insert([
            [
                'name' => 'Strategy 1',
                'description' => $faker->text(100)
            ],
            [
                'name' => 'Strategy 2',
                'description' => $faker->text(100)
            ],
            [
                'name' => 'Strategy 3',
                'description' => $faker->text(100)
            ]
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('strategies');
        Schema::dropIfExists('binance_accounts');
    }
}
