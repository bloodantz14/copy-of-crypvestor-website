#!/bin/bash

# dont forget to add this commnads to visudo so www-data can access to sudo without password.
echo " -- supervisorctl reread --"
sudo supervisorctl status all

echo " -- Supervisorctl Update --"
sudo supervisorctl update all

echo " -- Supervisorctl Restart --"
sudo supervisorctl restart all

echo " -- Supervisorctl Status --"
sudo supervisorctl status all

echo " -- Done --"
