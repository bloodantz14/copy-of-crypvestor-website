<?php

use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\OAuthController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Auth\UserController;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Controllers\Settings\PasswordController;
use App\Http\Controllers\Settings\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BinanceController;
use App\Http\Controllers\BinanceAdminController;
use App\Http\Controllers\GenericController;
use App\Http\Controllers\StatusController;
use App\Http\Controllers\FuturesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', [LoginController::class, 'logout']);

    Route::get('user', [UserController::class, 'current']);

    Route::patch('settings/profile', [ProfileController::class, 'update']);
    Route::patch('settings/password', [PasswordController::class, 'update']);

    Route::post('dashboard/add-account', [BinanceController::class, 'addAccount']);
    Route::post('dashboard/update-account', [BinanceController::class, 'updateAccount']);
    Route::delete('dashboard/delete-account/{binance_account_id}', [BinanceController::class, 'deleteAccount']);
    Route::get('dashboard/closed-orders-client', [BinanceController::class, 'getClosedOrdersClient']);
    Route::get('dashboard/pnl/date/{start_date}/binance-account/{binance_account_id?}', [BinanceController::class, 'getPnlLastMonth']);
    Route::get('get-friends', [BinanceController::class, 'getFriends']);
});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', [LoginController::class, 'login']);
    Route::post('register', [RegisterController::class, 'register']);

    Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail']);
    Route::post('password/reset', [ResetPasswordController::class, 'reset']);

    Route::post('email/verify/{user}', [VerificationController::class, 'verify'])->name('verification.verify');
    Route::post('email/resend', [VerificationController::class, 'resend']);

    Route::post('oauth/{driver}', [OAuthController::class, 'redirect']);
    Route::get('oauth/{driver}/callback', [OAuthController::class, 'handleCallback'])->name('oauth.callback');
});


Route::get('last-prices', [BinanceController::class, 'getLastPrices']);
Route::get('strategies', [BinanceController::class, 'getStrategies']);
Route::post('contact', [GenericController::class, 'sendContactMessage']);
Route::get('binance-accounts', [BinanceController::class, 'getBinanceAccounts']);
Route::get('order-statuses', [BinanceController::class, 'getOrderStatues']);
Route::get('binance-account/api-data/{binance_account_id}', [BinanceController::class, 'getBinanceAccountData']);
Route::get('pnl-last-30-days/strategy-1', [BinanceController::class, 'getPnlLast30Days']);



Route::prefix('admin')
    ->middleware('admin')
    ->group(function () {

        //Route::get('binance-accounts', [BinanceAdminController::class, 'getBinanceAccounts']);
        //Route::get('binance-accounts/api-data', [BinanceAdminController::class, 'getBinanceAccountsData']);
        Route::get('order-rules', [BinanceAdminController::class, 'getOrderRules']);
        Route::get('order-groups', [BinanceAdminController::class, 'getOrderGroups']);

        Route::post('orders/{open_order_id}/sell-now', [BinanceAdminController::class, 'sellOrderNow']);

        Route::post('pre-orders', [BinanceAdminController::class, 'storePreOrders']);
        Route::get('pre-orders', [BinanceAdminController::class, 'getPreOrders']);
        Route::post('pre-orders/delete-all', [BinanceAdminController::class, 'deleteAllPreOrders']);
        Route::delete('pre-orders/{pre_order_id}', [BinanceAdminController::class, 'deletePreOrders']);

        Route::get('open-orders', [BinanceAdminController::class, 'getOpenOrders']);
        Route::delete('open-orders/{open_order_id}', [BinanceAdminController::class, 'deleteOpenOrders']);
        Route::patch('open-orders/{open_order_id}', [BinanceAdminController::class, 'updateOpenOrders']);
        Route::post('open-orders/sell-all', [BinanceAdminController::class, 'sellAllOpenOrders']);
        Route::post('open-orders-bulk-update', [BinanceAdminController::class, 'updateOpenOrdersBulk']);

        Route::get('closed-orders', [BinanceAdminController::class, 'getClosedOrders']);
        Route::get('closed-orders/pnl/date/{start_date}/binance-account/{binance_account_id?}', [BinanceAdminController::class, 'getPnl']);

        Route::get('get-clients', [BinanceAdminController::class, 'getClients']);
        Route::get('get-clients-all', [BinanceAdminController::class, 'getClientsAll']);
        Route::get('get-client-accounts-api/{binance_account_id}', [BinanceAdminController::class, 'getClientBinanceAccountAPI']);

        Route::get('supervisor-status', [StatusController::class, 'getSupervisorStatus']);
        Route::post('restart-processes', [StatusController::class, 'restartProcesses']);
        Route::get('get-log-last-prices', [StatusController::class, 'getLogLastPrices']);

        Route::get('futures/income', [FuturesController::class, 'getFuturesIncome']);

});

